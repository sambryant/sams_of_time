from core import *

class TestSubBound():



  def test_outer(self):
    rect = ((0, 0), CHAR_SIZE, CHAR_SIZE)

    sb = SubBound(rect)

    assert sb.center_relative[0] == 0.0
    assert sb.center_relative[1] == 0.0
    assert sb.half_size[0] == 1.0
    assert sb.half_size[1] == 1.0
