from __future__ import annotations

import os, json
from typing import Dict, List, Any

import make_animations
from bounds import Bound
from core import *

class IO():

  def __init__(self, character_name: str) -> None:
    self.character_name = character_name

  def get_dir(self) -> str:
    return './%s' % self.character_name

  def get_sprite_sheet(self, action_name: str) -> str:
    return os.path.join(self.get_dir(), 'sprites_%s.png' % action_name)

  def get_sprite_data(self) -> str:
    return os.path.join(self.get_dir(), 'sprites.json')

  def get_animation_data(self) -> str:
    return os.path.join(self.get_dir(), 'animations.json')

  def get_bounds_data_filename(self) -> str:
    return os.path.join(self.get_dir(), 'bounds.json')

  def get_component_data(self) -> str:
    return os.path.join(self.get_dir(), 'info.json')


class Action():
  parent: Character
  name: str
  num_frames: int
  color: Color

  def __init__(self, parent: Character, name: str, color: Color, num_frames: int) -> None:
    self.parent = parent
    self.name = name
    self.num_frames = num_frames
    self.color = color

  def get_sprite_sheet_name(self) -> str:
    return self.parent.io.get_sprite_sheet(self.name)

  def get_directional_animation_key(self) -> str:
    return '%s-%s' % (self.parent.name, self.name)

  def get_animation_key(self, direction: Direc) -> str:
    return '%s-%s-%s' % (self.parent.name, self.name, direction)

  def get_sprite_key(self, direction: Direc, frame: int) -> str:
    return '%s-%s-%s-%d' % (self.parent.name, self.name, str(direction), (frame+1))

  def make_sprite_sheet(self) -> None:
    make_animations.make_image(
      self.get_sprite_sheet_name(),
      self.parent.body,
      self.parent.head,
      self.color,
      self.num_frames)

  def get_sprite_data(self) -> Dict:
    return {
      'filename': self.get_sprite_sheet_name(),
      'rows': len(Direc),
      'cols': self.num_frames,
      'assets': [
        {
          'key': '%s' % self.get_sprite_key(direction, frame),
          'x_index': frame,
          'y_index': int(direction.value),
          'width': 1,
          'height': 1
        } for direction in Direc for frame in range(self.num_frames)
      ]
    }

  def get_animation_data(self) -> List[Dict[str, Any]]:
    return [
      {
        'key': self.get_animation_key(direction),
        'sprites': [
          self.get_sprite_key(direction, frame) for frame in range(self.num_frames)
        ]
      } for direction in Direc
    ]

  def get_directional_animation_data(self) -> Dict[str, Any]:
    d = {
      'key': self.get_directional_animation_key(),
    }
    for direction in Direc:
      d[str(direction)] = self.get_animation_key(direction)
    return d


class Character():
  name: str
  actions: List[Action]
  body: Rect
  head: Rect
  io: IO
  bound: Bound

  def __init__(self, name: str, head: Rect, body: Rect) -> None:
    self.name = name
    self.head = head
    self.body = body
    self.actions = []
    self.io = IO(self.name)
    self.bound = Bound(self)
    self.bound.add_rect(body)
    self.bound.add_rect(head)

  def add_action(self, name: str, color: Color, num_frames: int) -> None:
    a = Action(self, name, color, num_frames)
    self.actions.append(a)

  def build(self) -> None:
    os.mkdir(self.io.get_dir())
    sprite_info: Dict[str, Any] = {'spritesheets': []}
    animation_info: Dict[str, Any] = {'directional_animations': [], 'animations': []}

    def print_dict(d, level=0):
      for key, value in d.items():
        if isinstance(value, dict):
          print('%s"%s": {' % (' '*level, key))
          print_dict(value, level + 2)
          print('%s}' % (' '*level))
        else:
          print('%s"%s": %s,' % (' '*level, key, str(type(value))))

    for action in self.actions:
      action.make_sprite_sheet()
      sprite_info['spritesheets'].append(action.get_sprite_data())
      animation_info['animations'].extend(action.get_animation_data())
      animation_info['directional_animations'].append(action.get_directional_animation_data())


    with open(self.io.get_sprite_data(), 'w') as f:
      f.write(json.dumps(sprite_info, indent=2))
    with open(self.io.get_animation_data(), 'w') as f:
      f.write(json.dumps(animation_info, indent=2))
    with open(self.io.get_bounds_data_filename(), 'w') as f:
      f.write(json.dumps(self.bound.get_data(), indent=2))

if __name__ == '__main__':
  WALK_COLOR = (48, 96, 130)
  ROLL_COLOR = (95, 205, 228)
  ATTK_COLOR = (217, 87, 99)
  BASE_COLOR = (238, 195, 154)
  HEAD_RECT = ((27, 15), 10, 10) # x1, y1, width, height
  BODY_RECT = ((16, 25), 32, 32)

  c = Character('minman', HEAD_RECT, BODY_RECT)
  c.add_action('base', BASE_COLOR, 5)
  c.add_action('roll', ROLL_COLOR, 5)
  c.add_action('walk', WALK_COLOR, 5)
  c.add_action('attack', ATTK_COLOR, 5)
  c.build()



