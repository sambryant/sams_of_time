from __future__ import annotations

import os, json
from typing import Dict, List, Any

import make_animations
from core import *


class SubBound():
  center_relative: Tuple[float, float]
  half_size: Tuple[float, float]

  def __init__(self, rect: Rect) -> None:
    # compute middle pixel
    px = rect[0][0] + rect[1]/2.0
    py = rect[0][1] + rect[2]/2.0
    # translate so middle is 0,0
    px = px - CHAR_SIZE/2.0
    py = py - CHAR_SIZE/2.0
    # convert to logical units
    cx = px / PIXELS_PER_UNIT
    cy = py / PIXELS_PER_UNIT
    self.center_relative = (cx, cy)

    half_width = (rect[1]/2.0)/PIXELS_PER_UNIT;
    half_height = (rect[2]/2.0)/PIXELS_PER_UNIT
    self.half_size = (half_width, half_height)

  def get_data(self) -> Dict[str, Any]:
    return {
      'center_relative': [self.center_relative[0], self.center_relative[1]],
      'half_size': [self.half_size[0], self.half_size[1]]
    }

class Bound():
  parent: Character
  outer: SubBound
  inner: List[SubBound]

  def __init__(self, parent: Character) -> None:
    self.parent = parent
    self.outer = SubBound(((0, 0), CHAR_SIZE, CHAR_SIZE))
    self.inner = []

  def add_rect(self, rect: Rect) -> None:
    self.inner.append(SubBound(rect))

  def get_data(self) -> Dict[str, Any]:
    return {
      '%s-%s' % (self.parent.name, 'bound'): {
        'outer': self.outer.get_data(),
        'inner': [
          i.get_data() for i in self.inner
        ]
      }
    }

def test_outer():
  rect = ((0, 0), CHAR_SIZE, CHAR_SIZE)

  sb = SubBound(rect)

  assert sb.center_relative[0] == 0.0
  assert sb.center_relative[1] == 0.0
  assert sb.half_size[0] == 1.0
  assert sb.half_size[1] == 1.0

if __name__ == '__main__':
  test_outer()
