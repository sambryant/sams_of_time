import warnings
import numpy as np
from typing import List, Tuple
from PIL import Image

Pixel = Tuple[int, int]

class Image32():
  pixels: List[Pixel] # list of pixels in image to track rotation for

  def __init__(self) -> None:
    self.pixels = []

  def save(self, img_name: str) -> None:
    img = Image.new('RGB', (32, 32), color='white')
    for pixel in self.pixels:
      img.putpixel(pixel, (0, 0, 0))
    img.save(img_name)

  def has_pixel(self, pixel: Pixel) -> bool:
    for pt in self.pixels:
      if pt[0] == pixel[0] and pt[1] == pixel[1]:
        return True
    return False

  def add_pixel(self, pixel: Pixel) -> None:
    if not Image32.is_valid(pixel):
      raise Exception('Arguments must be integer pixel index between 0 and 31 inclusive')
    self.pixels.append(pixel)

  @staticmethod
  def is_valid(pixel: Pixel) -> bool:
    for px in pixel:
      if not isinstance(px, int) or (px < 0 or px > 31):
        return False
    return True

  def get_rotation(self, angle: float) -> 'Image32':
    # def pixel_to_pt(pixel_pt: Tuple[int, int]):
    #   # 0 -> -15.5
    #   return np.array([float(pixel_pt[0]) - 15.5, float([pixel_pt[1]]) - 15.5])

    # convert list of integer pixels to list of float coordinates
    pts = [np.array(pixel) for pixel in self.pixels]

    # Translate so origin is in between pixel 15 and 16 so that pixels are located at half integers
    trans = np.array([-15.5, -15.5])
    pts = [pt + trans for pt in pts]

    # Rotation points by given angle
    rot = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
    pts = [np.matmul(rot, pt) for pt in pts]

    # Reverse original translation back into (0, 32) range
    pts = [pt - trans for pt in pts]

    # Convert back into integers and check if any points are clipped
    new_img = Image32()
    for pt in pts:
      pixel = (int(np.round(pt[0])), int(np.round(pt[1])))
      if not Image32.is_valid(pixel):
        warnings.warn('Some pixels were clipped after rotation')
        continue
      if new_img.has_pixel(pixel):
        warnings.warn('Distinct pixels merged after rotation')
        continue

      new_img.add_pixel(pixel)
    return new_img



def sword_test() -> None:
  img = Image32();
  # hilt
  img.add_pixel((15, 31))
  img.add_pixel((16, 31))
  img.add_pixel((15, 28))
  img.add_pixel((16, 28))
  # guard
  img.add_pixel((12, 25))
  img.add_pixel((19, 25))
  img.add_pixel((12, 27))
  img.add_pixel((19, 27))
  # blade sick
  img.add_pixel((13, 0))
  img.add_pixel((18, 0))
  img.add_pixel((13, 24))
  img.add_pixel((18, 24))

  img.save('new_sword/orig.png')
  step_size = (np.pi/2)/6;

  # img.get_rotation(np.pi/2).save('new_sword/left.png')
  for i in range(24):
    img.get_rotation(i*step_size).save('new_sword/frame%02d.png' % i)

if __name__ == '__main__':
  sword_test()
