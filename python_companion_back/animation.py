from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

from sprite import Sprite


class Animation():
  pass


class RegAnimation(Animation):
  frames: List[Sprite]
  def __init__(self, frames: List[Sprite]) -> None:
    self.frames = frames

class DirAnimation(Animation):
  frames: List[(Sprite, Sprite, Sprite, Sprite)]
  def __init__(self, u: List[Sprite], d: List[Sprite], l: List[Sprite], r: List[Sprite]) -> None:
    size = len(u)
    for frames in [u, d, l, r]:
      if len(frames) != size:
        raise Exception('DirAnimation has different number of frames for different directions')
    self.frames = [(u[i],d[i],l[i],r[i]) for i in range(size)]

class Sprite():
  width: int
  height: int
  image: GdkPixbuf.Pixbuf

  def __init__(self, image: GdkPixbuf.Pixbuf) -> None:
    self.image = image
    self.width = image.get_width()
    self.height = image.get_height()
