import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
import cairo

import argparse
import pkgutil
import sys
from typing import Any, List, Optional, Dict, Set, TypeVar, Type, Callable, Tuple, cast

from sprite_manager import SpriteManager
from animation_manager import AnimationManager
from map_manager import MapManager
from map import Map
from sprite import Sprite
from animation import Animation

_T = TypeVar('_T', bound=GObject.Object)

def _extract_object(builder: Gtk.Builder, name: str, cls: Type[_T]) -> _T:
  obj = builder.get_object(name)
  assert isinstance(obj, cls), "obj %s has type %s" % (obj, type(obj))
  return obj

@Gtk.Template.from_file('world-builder.ui')
class WorldBuilder(Gtk.ApplicationWindow):
  __gtype_name__ = 'WorldBuilder'
  main_paned = cast(Gtk.Paned, Gtk.Template.Child())
  content_paned = cast(Gtk.Paned, Gtk.Template.Child())
  map_image = cast(Gtk.Image, Gtk.Template.Child())
  map_list_model = cast(Gtk.ListStore, Gtk.Template.Child())
  map_list_view = cast(Gtk.TreeView, Gtk.Template.Child())
  tile_map_model = cast(Gtk.ListStore, Gtk.Template.Child())
  tile_map_view = cast(Gtk.TreeView, Gtk.Template.Child())
  tile_map_preview = cast(Gtk.Image, Gtk.Template.Child())
  tile_layer_model = cast(Gtk.ListStore, Gtk.Template.Child())
  tile_layer_view = cast(Gtk.TreeView, Gtk.Template.Child())

  # Flags
  flag_draw_grid: bool
  flag_show_selected: bool


  selected_tile: Optional[Tuple[int, int]]
  map_manager: MapManager
  sprite_manager: SpriteManager
  animation_manager: AnimationManager
  world_map: Optional[Map]
  map_pixbuf: Optional[GdkPixbuf.Pixbuf]

  def __init__(self) -> None:
    super().__init__()
    self.flag_draw_grid = True
    self.flag_show_selected = True

    self.main_paned.set_property('position', 200)
    self.content_paned.set_property('position', 500)

    self.sprite_manager = SpriteManager()
    self.animation_manager = AnimationManager(self.sprite_manager)
    self.map_manager = MapManager(self.sprite_manager)
    self.world_map = None
    self.map_pixbuf = None
    self.selected_tile = None
    self._fill_map_list()

  def _fill_map_list(self) -> None:
    self.map_list_model.clear()
    for name in self.map_manager.list_maps():
      self.map_list_model.append([name])

  @Gtk.Template.Callback('map-selection-changed')
  def _on_map_selection_changed(self, *args):
    model, itr = self.map_list_view.get_selection().get_selected()
    if itr:
      self.set_map(self.map_manager.get(model[itr][0]))
    else:
      self.set_map(None)

  @Gtk.Template.Callback('tile-map-selection-changed')
  def _on_tile_map_tile_selection_changed(self, *_: Any):
    model, itr = self.tile_map_view.get_selection().get_selected()
    if itr:
      key = model[itr][1]
      self.tile_map_preview.set_from_pixbuf(self.sprite_manager.get_sprite(key).get_image())
    else:
      self.tile_map_preview.set_from_pixbuf(None)

  @Gtk.Template.Callback('tile-layer-selection-changed')
  def _on_tile_layer_selection_changed(self, *_: Any) -> None:
    pass

  @Gtk.Template.Callback('map-image-button-press')
  def _on_map_image_clicked(self, ev_box: Gtk.EventBox, ev_but: Gdk.EventButton) -> None:
    if ev_but.type == Gdk.EventType.BUTTON_PRESS:
      # Determine which tile click happened at
      tw, th = self.world_map.get_tile_size()
      w, h = self.world_map.get_pixel_size()

      x, y = ev_but.x, ev_but.y

      if x > w or y > h:
        return

      self._set_selected_tile((int(x/tw), int(y/th)))

  def _set_selected_tile(self, pos: Optional[Tuple[int, int]]) -> None:
    if pos:
      print('Selecting tile: %d, %d' % (pos[0], pos[1]))
    else:
      print('Selecting tile: None')
    self.selected_tile = pos
    self.tile_layer_model.clear()
    if self.selected_tile and self.world_map:
      for i, sprite in enumerate(self.world_map.get_sprites_at(pos[0], pos[1])):
        self.tile_layer_model.append([i, sprite.key])
    self.redraw_map()

  def set_map(self, world_map: Optional[Map]) -> None:
    if world_map == self.world_map:
      return

    self.world_map = world_map

    self.tile_map_model.clear()
    if self.world_map:
      for id, sprite in self.world_map.get_tile_map().items():
        self.tile_map_model.append([id, sprite.key])
    self.redraw_map()

  def redraw_map(self) -> None:
    if self.world_map:
      tw, th = self.world_map.get_tile_size()
      w, h = self.world_map.get_pixel_size()
      # self.map_pixbuf = self.world_map.get_image().copy()

      frmt = cairo.Format.ARGB32
      surf = cairo.ImageSurface(frmt, w, h)
      cr = cairo.Context(surf)
      self.world_map.draw(cr)

      if self.flag_draw_grid:

        # Draw grid lines
        cr.set_source_rgb(0.5, 0.5, 0.5)
        x = 0
        while x < w:
          cr.move_to(x, 0.0)
          cr.line_to(x, h)
          cr.stroke()
          x += tw
        y = 0
        while y < h:
          cr.move_to(0.0, y)
          cr.line_to(w, y)
          cr.stroke()
          y += th

      if self.flag_show_selected and self.selected_tile:
        # Draw grid lines
        Gdk.cairo_set_source_rgba(cr, Gdk.RGBA(1.0, 1.0, 0.0, 0.5))
        x, y = self.selected_tile[0]*tw, self.selected_tile[1]*th
        print('Drew rect at %d,%d (%d, %d)' % (x,y,tw, th))
        cr.rectangle(x, y, tw, th)
        cr.fill()


      self.map_image.set_from_pixbuf(Gdk.pixbuf_get_from_surface(surf, 0, 0, w, h))

      # self.map_image.set_from_pixbuf(self.world_map.get_image())
    else:
      self.map_image.set_from_pixbuf(None)


class Application(Gtk.Application):
  world_builder: WorldBuilder

  def __init__(self) -> None:
    Gtk.Application.__init__(self)
    self.world_builder = WorldBuilder()

  def do_startup(self) -> None:
    Gtk.Application.do_startup(self)
    self.world_builder.set_application(self)

  def do_activate(self) -> None:
    self.world_builder.present()

  def run(self, argv: List[str]=[]) -> int:
    return super().run(argv)


def main() -> int:
  return Application().run()

if __name__ == '__main__':
  main()
