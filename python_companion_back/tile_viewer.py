import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
import cairo

import argparse
import pkgutil
import sys
from typing import Any, List, Optional, Dict, Set, TypeVar, Type, Callable, Tuple, cast

from sprite import Sprite
from sprite_manager import SpriteManager


@Gtk.Template.from_file('tile-viewer.ui')
class TileViewer(Gtk.Box):
  __gtype_name__ = 'TileViewer'
  __gsignals__ = {
    'selection-changed': (GObject.SignalFlags.RUN_FIRST, None, ())
  }

  tile_model = cast(Gtk.ListStore, Gtk.Template.Child())
  tile_view = cast(Gtk.TreeView, Gtk.Template.Child())
  tile_preview = cast(Gtk.Image, Gtk.Template.Child())

  _sprite_manager: SpriteManager
  _ignore_selection_change: bool

  def __init__(self, sprite_manager: SpriteManager) -> None:
    super().__init__()
    self._sprite_manager = sprite_manager
    self._ignore_selection_change = False

  def get_selected(self) -> Optional[Tuple[int, str]]:
    model, itr = self.tile_view.get_selection().get_selected()
    if itr:
      return model[itr][0], model[itr][1]
    else:
      None

  def set_model(self, sprites: List[Sprite]) -> None:
    self._ignore_selection_change = True
    self.tile_view.set_model(None)
    self.tile_model.clear()
    for i, spr in enumerate(sprites):
      self.tile_model.append([i, spr.key])
    self.tile_view.set_model(self.tile_model)
    self._ignore_selection_change = False
    self._on_tile_selection_changed()

  @Gtk.Template.Callback('tile-selection-changed')
  def _on_tile_selection_changed(self, *_: Any):
    if self._ignore_selection_change:
      return
    model, itr = self.tile_view.get_selection().get_selected()
    if itr:
      key = model[itr][1]
      self.tile_preview.set_from_pixbuf(self._sprite_manager.get_sprite(key).get_image())
    else:
      self.tile_preview.set_from_pixbuf(None)

    self.emit('selection-changed')



