from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

import collections
import json
import os
from typing import List, Dict, Optional, Any

class Rect():
  x: int
  y: int
  width: int
  height: int

  def __init__(self, x: int, y: int, width: int, height: int):
    self.x = x
    self.y = y
    self.width = width
    self.height = height

class Sprite():
  _sprite_sheet: SpriteSheet
  _region: Rect

  def __init__(self, spritesheet: SpriteSheet, x_ind: int, y_ind: int, ncols: int, nrows: int):
    self._sprite_sheet = spritesheet
    self._region = Rect(
      x_ind * spritesheet._col_size,
      y_ind * spritesheet._row_size,
      ncols * spritesheet._col_size,
      nrows * spritesheet._row_size)

  def get_size(self):
    return self._region.width, self._region.height

class SpriteSheet():
  _filename: str
  _image: GdkPixbuf
  _num_rows: int
  _num_cols: int
  _row_size: int
  _col_size: int
  _sprites: Dict[str, Sprite]

  def __init__(self, parent_dir: str, filename: str, num_rows: int, num_cols: int):
    self._filename = filename
    self._num_rows = num_rows
    self._num_cols = num_cols
    self._sprites = {}
    self._image = GdkPixbuf.Pixbuf.new_from_file(os.path.join(parent_dir, self._filename))

    w, h = self._image.get_width(), self._image.get_height()
    if w % num_cols != 0:
      raise Exception('Sprite sheet width (%d) not a multiple of num cols (%d): "%s"' % (w, self._num_cols, filename))
    self._col_size = int(w / num_cols)
    if h % self._num_rows != 0:
      raise Exception('Sprite sheet height (%d) not a multiple of num rows (%d): "%s"' % (h, self._num_rows, filename))
    self._row_size = int(h / num_rows)

  @classmethod
  def from_json(cls, parent_dir: str, json_dict: object[String, Any]) -> SpriteSheet:
    ss = SpriteSheet(parent_dir, json_dict['filename'], json_dict['rows'], json_dict['cols'])
    for asset in json_dict['assets']:
      name, x, y, w, h = asset['key'], asset['x_index'], asset['y_index'], asset['width'], asset['height']
      ss._sprites[name] = Sprite(ss, x, y, w, h)
    return ss


class ResourceManager():
  _INSTANCE = None
  _sprite_sheets: List[str]
  _sprite_map: Dict[str, Sprite]

  def __init__(self) -> None:
    self._sprite_sheets = []
    self._sprite_map = {}

  @staticmethod
  def get() -> ResourceManager:
    if not ResourceManager._INSTANCE:
      ResourceManager._INSTANCE = ResourceManager()
      ResourceManager._INSTANCE.add_resource_dir('../assets/tilesets')
      ResourceManager._INSTANCE.add_resource_dir('../assets/sprites')
    return ResourceManager._INSTANCE

  def get_sprite(self, key: str) -> Sprite:
    return self._sprite_map[key]

  def add_resource_dir(self, resource_dir: str) -> None:
    json_filename = os.path.join(resource_dir, 'info.json')
    json_dict = json.loads(open(json_filename, 'r').read())
    for ss_dict in json_dict['spritesheets']:
      ss = SpriteSheet.from_json(resource_dir, ss_dict)
      self._sprite_sheets.append(ss)
      self._sprite_map.update(ss._sprites)

if __name__ == '__main__':
  rm = ResourceManager()
  rm.add_resource_dir('../assets/tilesets')
  rm.add_resource_dir('../assets/sprites')
  print('Found %d tiles' % len(rm._sprite_map.keys()))


