use serde::de::DeserializeOwned;
use std::path::Path;

mod entity_manager;
mod entity_raw;

pub use entity_manager::LoadableEntity;
pub use entity_manager::EntityManager;

fn load_json_asset<T, P>(filename: P) -> Result<T, String> where
  T: DeserializeOwned,
  P: AsRef<Path> + std::fmt::Display
{
  let contents = std::fs::read_to_string(&filename)
    .map_err(|e| {
      eprintln!("Debug: root error: {:?}", e);
      format!("Error in reading file: {}", filename)
    })?;
  let object: T = serde_json::from_str(&contents)
    .map_err(|e| {
      eprintln!("Debug: root error: {:?}", e);
      format!("Error in parsing file as json object: {}", filename)
    })?;
  Ok(object)
}
