#![allow(unused_variables, unused_imports)]
use std::collections::HashMap;
use specs::prelude::*;

use crate::components::*;
use crate::resources::TextureManager;
use super::{
  entity_raw::{
    ActionData,
    BoundData,
    EntityData,
    EntityListData,
    SubBoundData
  },
  load_json_asset
};

pub trait LoadableEntity: Sized {
  fn load_from(self, texture_manager: &TextureManager, entity_manager: &EntityManager, key: &str) -> Result<Self, String>;
}

impl<'a> LoadableEntity for EntityBuilder<'a> {
  fn load_from(self, texture_manager: &TextureManager, entity_manager: &EntityManager, key: &str) -> Result<EntityBuilder<'a>, String> {
    let data = entity_manager.entity_map.get(key)
      .ok_or(format!("Invalid entity key: {}", key))?;

    let mut builder = self
      .with(Physics::new(data.max_speed))
      .with(Bound::from(&data.bounds))
      .with(action_from_data(&data.actions, texture_manager)?)
      .with(texture_manager.get_default_sprite());

    if data.rigid {
      builder = builder.with(Rigid {});
    }
    Ok(builder)
  }
}


pub struct EntityManager {
  entity_map: HashMap<String, EntityData>
}

pub enum EntityError {
  FileReadError(String),
  FileFormatError(String),
  AssetError,
  DupeError(String)
}

impl EntityManager {

  pub fn new(texture_manager: &mut TextureManager) -> Result<EntityManager, String> {
    let mut em = EntityManager { entity_map: HashMap::new() };
    em.load_entities(texture_manager, "new_assets/entities")?;
    Ok(em)
  }

  pub fn print_loaded_content(&self) {
    println!("Entity manager: {} entities", self.entity_map.len());
    for (entity, _) in &self.entity_map {
      println!("\t{}", entity);
    }
  }

  fn load_entity(&mut self, texture_manager: &mut TextureManager, entity_dir: &str) -> Result<(), String> {
    let entity_data: EntityData = load_json_asset(format!("{}/info.json", entity_dir))?;
    let entity = String::from(&entity_data.name);
    texture_manager.add_texture_pack_file(&entity_dir, &entity_data.assets.sprites_file)
      .map_err(|e| {
        eprintln!("Texture error for entity {}: {}", entity, e);
        EntityError::AssetError
      })?;
    texture_manager.add_animation_pack_file(&entity_dir, &entity_data.assets.animations_file)
      .map_err(|e| {
        eprintln!("Animation error for entity {}: {}", entity, e);
        EntityError::AssetError
      })?;

    if !self.entity_map.contains_key(&entity) {
      self.entity_map.insert(entity, entity_data);
      Ok(())
    } else {
      Err(format!("Duplicate entity error: {}", entity))
    }
  }

  fn load_entities(&mut self, texture_manager: &mut TextureManager, base_dir: &str) -> Result<(), String> {
    let entity_list: EntityListData = load_json_asset(format!("{}/info.json", base_dir))?;

    for entity in entity_list.entities {
      let entity_dir = format!("{}/{}", base_dir, entity);
      self.load_entity(texture_manager, &entity_dir)?;
    }
    Ok(())
  }
}

impl From<EntityError> for String {
  fn from(e: EntityError) -> String {
    match e {
      EntityError::FileReadError(fname) => format!("File read error: {}", fname),
      EntityError::FileFormatError(fname) => format!("File format error: {}", fname),
      EntityError::AssetError => String::from("Asset error"),
      EntityError::DupeError(key) => format!("Duplicate entity error: {}", key),
    }
  }
}

impl From<&BoundData> for Bound {
  fn from(bd: &BoundData) -> Bound {
    Bound {
      outer: (&bd.outer).into(),
      inner: Some(bd.inner.iter().map(|sbd| sbd.into()).collect())
    }
  }
}

impl From<&SubBoundData> for SubBound {
  fn from(sbd: &SubBoundData) -> SubBound {
    SubBound {
      center_relative: sbd.center_relative.into(),
      half_size: sbd.half_size.into()
    }
  }
}

pub fn action_from_data(data: &Vec<ActionData>, texture_manager: &TextureManager) -> Result<Action, String> {
  let mut builder = ActionBuilder::new(texture_manager);
  for action_data in data {
    let atype = match &action_data.atype[..] {
      "base" => ActionType::Default,
      "walk" => ActionType::Walk,
      "roll" => ActionType::Roll,
      "slash" => ActionType::Slash,
      _ => return Err(format!("Unknown action type: {}", action_data.atype))
    };
    let length = match action_data.length {
      None => ActionLength::Indefinite,
      Some(nframes) => ActionLength::Stretched(nframes)
    };
    let directional = true;
    builder = builder.add(
      atype,
      &action_data.animation,
      length,
      action_data.move_speed,
      directional,
      action_data.interruptable);
  }
  builder.build()
}
