use crate::components::*;
use specs::prelude::*;

pub struct CleanupSystem;

impl<'a> System<'a> for CleanupSystem {
  type SystemData = (
    WriteExpect<'a, CollisionQueue>,
  );

  fn run(&mut self, (mut collision_queue,): Self::SystemData) {
    collision_queue.clear();
  }
}
