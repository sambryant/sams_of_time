use crate::components::*;

pub struct ArcAttackData {
  animation: DirectionalAnimation,
  num_frames_duration: u32,
  bounds: Vec<NGon>, // bounding polygon of hit box for each frame.
}






/// Generates arc attack data including the correct bounding polygons from more basic arguments.
/// This is slow so should only be used during loading screens or even to just generate raw data.
pub fn gen_arc_attack_data(nframes: u32, radius: f32, theta_1: f32, theta_2: f32) {
  let slice_size = (theta_2 - theta_1)/(nframes as f32);

  let center = Point{ x: 0.0, y: 0.0 };

  let get_bounds = |frame: u32| -> NGon {
    let th1 = theta_1 + (frame as f32) * slice_size;
    let th2 = th1 + slice_size;
    vec!(
      center,
      Point {
        x: radius * th1.cos(),
        y: radius * th1.sin()},
      Point {
        x: radius * th2.cos(),
        y: radius * th2.sin()})
  };

  let bounds: Vec<NGon> = (0..nframes).map(|i| get_bounds(i)).collect();

  todo!();
}





