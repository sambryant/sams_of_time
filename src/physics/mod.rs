pub mod collision_system;
pub mod components;
pub mod movement_system;
pub mod update_position_system;
pub mod collision_math;

use specs::prelude::*;
use components::*;

pub fn register_all(world: &mut World) {
  world.register::<Position>();
  world.register::<Physics>();
  world.register::<Rigid>();
  world.register::<Bound>();
  world.register::<Orientation>();
}
