use core::fmt::Display;
use specs::prelude::*;
use std::ops::Mul;
use std::ops::AddAssign;
use std::ops::SubAssign;
use std::ops::Add;
use std::ops::Sub;

use crate::components::*;
use crate::components::Direction::*;

pub type Velocity = Vector;

pub type NGon = Vec<Point>;

// Component definitions

/// Component indicating that entity's bound cannot overlap with another rigid entity's bound.
pub struct Rigid;
impl Component for Rigid {
  type Storage = VecStorage<Self>;
}

/// Component storing entity's position in physical space.
pub struct Position {
  /// The position of the entity at the start of the frame.
  pub cur: Point,
  /// The position of the entity in the next frame (as suggested by MovementSystem) if no collision
  /// occurs.
  pub nxt: Point,
  /// Flag set in each frame indicating `nxt` and `cur` are different (entity is going to move).
  /// This is used to avoid having to determine collisions between pairs of immovable objects.
  pub moving_flag: bool // true if entity is proposed to move in an update cycle.
}
impl Component for Position {
  type Storage = VecStorage<Self>;
}

/// Component indicating bounds of entity in physical space.
pub struct Bound {
  /// Rough rectangle giving over-estimate of an entity's bound. Used to do a first pass collision
  /// check. If Entity's true bound is a rectangle, then this serves as the exact bound.
  pub outer: SubBound,
  /// List of rectangles comprising the exact bound of an entity. These are only used if `outer` is
  /// determined to collide with another entity's `outer`
  pub inner: Option<Vec<SubBound>>
}
/// Used to construct non-rectangular bounds by constructing bound from multiple SubBounds.
pub struct SubBound {
  /// Center of this bounding rectangle relative to the `Position` of the entity.
  pub center_relative: Point,
  /// width/2.0 and height/2.0 for this rectangular bound. The half dimensions are stored because of
  /// how collision math is implemented.
  pub half_size: Dim
}
impl Component for Bound {
  type Storage = VecStorage<Self>;
}

pub struct Physics {
  /// Current velocity of entity in each frame.
  pub vel: Velocity,

  /// The max speed of an entity. This is just a suggestion and is often ignored (or buffed). This
  /// should eventually be put into another component or refactored somehow.
  pub max_speed: f32
}
impl Component for Physics {
  type Storage = VecStorage<Self>;
}

#[derive(Clone)]
pub struct Orientation {
  pub dir: Direction,
  x_norm: f32, // between 0.0 and 1.0 on unit circle
  y_norm: f32, // between 0.0 and 1.0 on unit circle
}
impl Component for Orientation {
  type Storage = VecStorage<Self>;
}

// Convenience implementations.

impl Orientation {
  pub fn new(x: f32, y: f32) -> Option<Orientation> {
    if (x == 0.0) & (y == 0.0) {
      None
    } else {
      let norm = (x*x + y*y).sqrt();
      let dir = if x.abs() > y.abs() {
        if x > 0.0 {
          Right
        } else {
          Left
        }
      } else {
        if y > 0.0 {
          Down
        } else {
          Up
        }
      };
      Some(Orientation {dir, x_norm: x/norm, y_norm: y/norm})
    }
  }
  pub fn from_angle(ang: f32) -> Orientation{
    let x_norm = ang.cos();
    let y_norm = ang.sin();
    let dir = if x_norm.abs() > y_norm.abs() {
      if x_norm > 0.0 {
        Right
      } else {
        Left
      }
    } else {
      if y_norm > 0.0 {
        Down
      } else {
        Up
      }
    };
    Orientation {dir, x_norm, y_norm}
  }
}

impl Physics {
  pub fn new(max_speed: f32) -> Physics {
    Physics {
      vel: Velocity {x: 0.0, y: 0.0},
      max_speed
    }
  }

  pub fn set_velocity(&mut self, orientation: &Orientation, speed_modifier: f32) {
    self.vel.x = self.max_speed * speed_modifier * orientation.x_norm;
    self.vel.y = self.max_speed * speed_modifier * orientation.y_norm;
  }

}

impl Vector {
  pub fn new(x: f32, y: f32) -> Vector {
    Point{x: x, y: y}
  }
}
impl Add for Vector {
  type Output = Self;

  #[inline(always)]
  fn add(self, other: Self) -> Self {
    Point {x: self.x + other.x, y: self.y + other.y}
  }
}
impl Sub for Vector {
  type Output = Self;

  #[inline(always)]
  fn sub(self, other: Self) -> Self {
    Point {x: self.x - other.x, y: self.y - other.y}
  }
}
impl Mul<f32> for Vector {
  type Output = Self;
  #[inline(always)]
  fn mul(self, other: f32) -> Self {
    Vector{x: self.x * other, y: self.y * other}
  }
}
impl SubAssign for Vector {
  #[inline(always)]
  fn sub_assign(&mut self, other: Self) {
    *self = Self {
      x: self.x - other.x,
      y: self.y - other.y
    }
  }
}
impl AddAssign for Vector {
  #[inline(always)]
  fn add_assign(&mut self, other: Self) {
    *self = Self {
      x: self.x + other.x,
      y: self.y + other.y
    }
  }
}
impl Add for Dim {
  type Output = Dim;

  #[inline(always)]
  fn add(self, other: Dim) -> Dim{
    Dim {width: self.width + other.width, height: self.height + other.height}
  }
}
impl SubBound {
  pub fn from_outer(outer: Dim) -> SubBound {
    SubBound {
      center_relative: Point{x: 0.0, y: 0.0},
      half_size: Dim{width: outer.width, height: outer.height}
    }
  }
}

impl Bound {
  pub fn subbounds_or_outer(&self) -> Vec<&SubBound> {
    if let Some(inner) = &self.inner {
      if inner.len() > 0 {
        return inner.iter().collect()
      }
    }
    vec!(&self.outer)
  }
}

impl Position {
  pub fn new(x: f32, y: f32) -> Position {
    Position {
      cur: Point {x, y},
      nxt: Point {x, y},
      moving_flag: false
    }
  }
}

impl Dim {
  pub fn new(width: f32, height: f32) -> Dim {
    Dim {width, height}
  }
}

impl Bound {
  pub fn new(width: f32, height: f32) -> Bound {
    Bound {
      outer: SubBound {
        center_relative: Point::new(0.0, 0.0),
        half_size: Dim::new(width/2.0, height/2.0)
      },
      inner: Some(Vec::new())
    }
  }
}

impl Display for Orientation {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
    return self.dir.fmt(f)
  }
}

impl Display for Direction {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
    match self {
      Up => {
        write!(f, "Up")
      },
      Down => {
        write!(f, "Down")
      },
      Left => {
        write!(f, "Left")
      },
      Right => {
        write!(f, "Right")
      }
    }
  }
}