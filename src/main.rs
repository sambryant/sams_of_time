extern crate specs;
#[macro_use]
extern crate specs_derive;

pub mod actions;
pub mod attacks;
pub mod attack_arc;
pub mod ai;
pub mod game;
pub mod graphics;
pub mod input;
pub mod map;
pub mod physics;
pub mod systems;
pub mod resources;
pub mod loader;
pub mod test;
pub mod experimental;
pub mod components;

fn main() {
  test::main();
  // experimental::main();
  // systems::graphics::map::test();
  // resources::test();
}
