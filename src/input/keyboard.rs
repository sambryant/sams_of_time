use specs::prelude::*;
use sdl2::event;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

use crate::components::*;

pub struct KeyboardSystem {
  event_pump: sdl2::EventPump
}

impl KeyboardSystem {
  pub fn new(sdl: &mut sdl2::Sdl) -> KeyboardSystem {
    let ep = sdl.event_pump()
      .expect("Failed to get event pump");
    KeyboardSystem {event_pump: ep}
  }
}

fn handle_key_event(physics: &mut Physics, event: &Event) {
  /*
   * This is a stateless and overly simplistic. Ultimately we should save the ongoing player actions
   * and change the logic of this handler depending on that state.
   */
  match event {
    Event::KeyDown{timestamp: _, window_id: _, keycode, scancode: _, keymod: _, repeat: _} => {
      match keycode {
        Some(Keycode::W) => physics.vel.y = - physics.max_speed,
        Some(Keycode::A) => physics.vel.x = - physics.max_speed,
        Some(Keycode::S) => physics.vel.y =   physics.max_speed,
        Some(Keycode::D) => physics.vel.x =   physics.max_speed,
        _ => {}
      };
    },
    Event::KeyUp{timestamp: _, window_id: _, keycode, scancode: _, keymod: _, repeat: _} => {
      match keycode {
        Some(Keycode::W) => physics.vel.y = 0.0,
        Some(Keycode::A) => physics.vel.x = 0.0,
        Some(Keycode::S) => physics.vel.y = 0.0,
        Some(Keycode::D) => physics.vel.x = 0.0,
        _ => {}
      };
    },
    _ => {}
  }
}

impl<'a> System<'a> for KeyboardSystem {
  type SystemData = (
    ReadExpect<'a, Player>,
    WriteExpect<'a, GameState>,
    WriteStorage<'a, Physics>,
  );

  fn run(&mut self, (player, mut state, mut physics): Self::SystemData) {
    let mut phy = physics.get_mut(player.entity)
      .expect("Player entity lacks a physics component!");

    for event in self.event_pump.poll_iter() {
      match event {
        event::Event::Quit {..} => state.is_quit = true,
        _ => {
          handle_key_event(&mut phy, &event);
        }
      };
    }
  }
}