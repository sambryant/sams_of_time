use specs::prelude::*;

use crate::components::*;

pub const L_ANALOG_X: u32 = 0;
pub const L_ANALOG_Y: u32 = 1;
pub const R_ANALOG_X: u32 = 3;
pub const R_ANALOG_Y: u32 = 4;

pub const INTERACT: u32 = 0;
pub const ROLL: u32 = 1;
pub const ATTACK: u32 = 5;

pub struct JoystickSystem {
  joystick: sdl2::joystick::Joystick
}

impl JoystickSystem {
  pub fn new(sdl: &mut sdl2::Sdl) -> Result<JoystickSystem, String> {
    let jss = sdl.joystick()
      .expect("Failed to get joystick subsystem");

    let js = jss.open(0)
      .map_err(|_| String::from("No joystick found"))?;
    Ok(JoystickSystem {
      joystick: js
    })
  }
}

impl<'a> System<'a> for JoystickSystem {
  type SystemData = (
    ReadExpect<'a, Player>,
    WriteExpect<'a, GameState>,
    WriteStorage<'a, QueuedAction>,
    WriteStorage<'a, Action>,
    WriteStorage<'a, Physics>,
    WriteStorage<'a, Orientation>,
  );

  fn run(&mut self, (player, mut _state, mut action, mut physics, mut orientation): Self::SystemData) {
    let mut player_physics = physics.get_mut(player.entity)
      .expect("Player entity lacks a physics component!");
    let action = action.get_mut(player.entity)
      .expect("Player entity lacks an action component!");
    let orientation = orientation.get_mut(player.entity)
      .expect("Player entity lacks an orientation!");

    if action.is_interruptable() {
      let x = self.joystick.axis(L_ANALOG_X).unwrap() as f32;
      let y = self.joystick.axis(L_ANALOG_Y).unwrap() as f32;
      let new_orientation = Orientation::new(x, y);
      let mut moving = false;
      if let Some(o) = new_orientation {
        *orientation = o;
        moving = true;
      }

      let speed_modifier = if self.joystick.button(ROLL).unwrap() {
        println!("Roll!");
        // player_physics.set_velocity(&orientation, 2.0);
        action.set(ActionType::Roll)
      } else if self.joystick.button(ATTACK).unwrap() {
        // player_physics.set_velocity(&orientation, 0.0);
        println!("Slash!");
        action.set(ActionType::Slash)
        // action.set(ActionType::Slash).unwrap();
      } else if moving {
        action.set(ActionType::Walk)
        // player_physics.set_velocity(&orientation, 1.0);
        // action.set(ActionType::Walk).unwrap();
      } else {
        action.set(ActionType::Default)
        // player_physics.set_velocity(&orientation, 0.0);
        // action.set(ActionType::Default).unwrap();
      };
      player_physics.set_velocity(&orientation, speed_modifier);
    }
    // for b in 0..self.joystick.num_buttons() {
    //   if self.joystick.button(b).unwrap() {
    //     println!("Pressed: {}", b);
    //   }
    // }
    // action_queue.push(_player.entity, Action::R1Attack);
  }
}