use specs::prelude::*;
use std::fmt::{Display, Formatter};
use crate::resources::TextureManager;
use crate::components::*;

#[derive(Component)]
#[storage(HashMapStorage)]
pub struct QueuedAction {
  pub action_type: ActionType
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Action {
  current: ActionType,
  pub actions: Vec<Option<ActionData>>
}

#[derive(Clone, Copy)]
pub enum ActionType {
  Default = 0,
  Walk = 1,
  Roll = 2,
  Slash = 3
}
const ACTIONS_NUMBER: usize = 4;

pub enum ActionLength {
  /// Signals action should last for as many frames as the animation
  Animation,
  /// Signals action persists until explicitly ended,
  Indefinite,
  /// Signals action should last for the given number of frames. If the given number of frames
  /// differs from the length of the animation, the animation is stretched out.
  Stretched(u32),
  /// Signals action should last for given number of frames. If the given number of frames differs
  /// fromt he length of the animation, the animation is looped.
  Looped(u32),
}

pub struct ActionData {
  animation: Animation,
  animation_stretch: Option<f32>, // Factor to extend the animation frames by
  interruptable: bool,
  move_speed: f32, // movement speed during action as fraction of max_speed
  frame_length: Option<u32>,
  pub ellapsed_frames: u32,
}

impl Action {
  fn new() -> Action {
    let mut actions = Vec::new();
    for _ in 0..ACTIONS_NUMBER {
      actions.push(None)
    }
    Action {current: ActionType::Default, actions}
  }

  pub fn is_interruptable(&self) -> bool {
    self.actions[self.current as usize].as_ref().unwrap().interruptable
  }

  pub fn set(&mut self, action: ActionType) -> f32 {
    let mut data = self.actions[action as usize].as_mut().expect("Invalid action for this entity");
    if self.current != action {
      data.ellapsed_frames = 0;
      self.current = action;
    }
    data.move_speed
  }

  pub fn get_current_action(&self) -> &ActionData {
    self.actions[self.current as usize].as_ref()
      .expect("Missing action data for current action")
  }

  pub fn has_type(&self, action: ActionType) -> bool {
    if let Some(_) = self.actions.get(action as usize).unwrap() {
      true
    } else {
      false
    }
  }

  pub fn set_default(&mut self) {
    self.set(ActionType::Default);
  }

  pub fn get_type(&self) -> ActionType {
    self.current
  }

  pub fn update(&mut self) {
    let data = self.actions[self.current as usize].as_mut()
      .expect("Bug: Missing action data for current action");

    // Update number of ellapsed frames
    data.ellapsed_frames += 1;

    // Check if this action has a finite time before expiring
    if let Some(limit) = data.frame_length {
      if data.ellapsed_frames == limit {
        self.set_default();
        return
      }
    }
    // If this animation has a stretch factor, compute the effective frame which should be shown.
    if let Some(factor) = data.animation_stretch {
      data.animation.set_frame((data.ellapsed_frames as f32 * factor).floor() as usize);
    } else {
      data.animation.update()
    }
  }

  pub fn get_sprite(&self, direction: Direction) -> Sprite {
    self.actions[self.current as usize].as_ref()
      .expect("Bug: Missing action data for current action")
      .animation.get_sprite(direction)
  }

}

pub struct ActionBuilder<'a> {
  tm: &'a TextureManager,
  action: Action
}

impl<'a> ActionBuilder<'a> {
  pub fn new(tm: &'a TextureManager) -> ActionBuilder<'a> {
    ActionBuilder{tm, action: Action::new()}
  }

  pub fn add(
    mut self,
    action: ActionType,
    animation: &str,
    length: ActionLength,
    move_speed: f32,
    directional: bool,
    interruptable: bool) -> Self {
    if let ActionType::Default = action {
      return self.add_default(animation, directional)
    }

    // Retrieve animation of proper length
    let animation = match directional {
      true => Animation::Directional(self.tm.get_directional_animation(animation).unwrap()),
      false => Animation::Simple(self.tm.get_animation(animation).unwrap())
    };

    // Compute length of action
    let frame_length = match length {
      ActionLength::Indefinite => None,
      ActionLength::Stretched(frames) => Some(frames),
      ActionLength::Looped(frames) => Some(frames), // shocking that this syntax is valid
      ActionLength::Animation => Some(animation.len() as u32)
    };

    let animation_stretch = match length {
      ActionLength::Stretched(limit) => Some(animation.len() as f32 / limit as f32 ),
      _ => None
    };
    println!("Animation stretch: {:?}", animation_stretch);

    let data = ActionData {
      animation,
      animation_stretch,
      interruptable,
      frame_length,
      move_speed,
      ellapsed_frames: 0
    };

    self.action.actions[action as usize] = Some(data);
    self
  }

  pub fn add_default(mut self, animation: &str, is_directional: bool) -> Self {
    let animation = match is_directional {
      true => Animation::Directional(self.tm.get_directional_animation(animation).unwrap()),
      false => Animation::Simple(self.tm.get_animation(animation).unwrap())
    };
    let data = ActionData{
      animation,
      animation_stretch: None,
      interruptable: true,
      frame_length: None,
      move_speed: 0.0,
      ellapsed_frames: 0
    };
    self.action.actions[ActionType::Default as usize] = Some(data);
    self
  }

  pub fn build(self) -> Result<Action, String> {
    if let None = self.action.actions.get(0) {
      Err(String::from("Cannot build an action set without a default"))
    } else {
      Ok(self.action)
    }
  }
}

impl ActionType {
  pub fn is_default(&self) -> bool {
    if let ActionType::Default = self {
      true
    } else {
      false
    }
  }
}

impl Display for ActionType {
  fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
    match self {
      ActionType::Default => write!(f, "default"),
      ActionType::Walk => write!(f, "walk"),
      ActionType::Roll => write!(f, "roll"),
      ActionType::Slash => write!(f, "slash"),
    }
  }
}

impl PartialEq for ActionType {
  fn eq(&self, other: &Self) -> bool {
    *self as usize == *other as usize
  }
}
