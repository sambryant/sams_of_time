mod action_queue_system;
mod action_update_system;
pub mod components;

use specs::prelude::*;

pub use action_update_system::ActionUpdateSystem;
pub use action_queue_system::ActionQueueSystem;

pub fn register_all(world: &mut World) {
  world.register::<components::Action>();
  world.register::<components::QueuedAction>();
}
