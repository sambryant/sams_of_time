use specs::prelude::*;
use crate::components::*;

pub struct ActionQueueSystem;

impl<'a> System<'a> for ActionQueueSystem {
  type SystemData = (
    WriteStorage<'a, Action>,
    WriteStorage<'a, QueuedAction>,
    WriteStorage<'a, Physics>,
    WriteStorage<'a, Orientation>,
  );

  fn run(&mut self, (mut action, mut queued_action, mut physics, mut orientation): Self::SystemData) {
    for (a,) in (&mut action,).join() {
      a.update();
    }
  }
}
