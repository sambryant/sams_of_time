use specs::prelude::*;
use crate::components::*;

pub struct ActionSystem;

impl<'a> System<'a> for ActionSystem {
  type SystemData = (
    WriteStorage<'a, Action>,
  );

  fn run(&mut self, (mut action,): Self::SystemData) {
    for (a,) in (&mut action,).join() {
      a.update();
    }
  }
}
