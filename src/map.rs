use std::collections::HashMap;
use std::path::{PathBuf};
use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::render::Texture;
use sdl2::rect::Rect;
use serde::{Deserialize, Serialize};

use crate::components::*;
use crate::game::Game;
use crate::game::Config;
use crate::graphics::Region;
use crate::resources::TextureManager;

pub struct Map {
  region: Region,
  texture: Texture,
}

#[derive(Serialize, Deserialize)]
struct MapData {
  tile_key: Vec<(u32, String)>,
  x1: f32,
  y1: f32,
  background_layer: Vec<Vec<String>> // each string is ";"-delimited ordered list of tiles at that position.
}

impl Map {
  pub fn load_map(
      game: &mut Game,
      map_name: &str) -> Map {
    let data = MapData::load_map_data(map_name).unwrap();

    // Make a map from the tile id identifiers to the corresponding Sprite objects
    let sprite_map = data.get_sprite_map(game.config.physical_tile_size, &game.texture_manager)
      .expect(&format!("Error in loading map {}", map_name));

    // Determine number of rows/cols from background layer
    let nrows = data.background_layer.len();
    let ncols = if nrows != 0 {
      data.background_layer[0].len()
    } else {
      panic!(format!("Error in loading map {}: map appears empty", map_name));
    };
    if ncols == 0 {
      panic!(format!("Error in loading map {}: map appears empty", map_name));
    }

    let canvas_width = ncols as u32 * game.config.physical_tile_size;
    let canvas_height = nrows as u32 * game.config.physical_tile_size;
    let texture = game.texture_manager.make_new_texture(canvas_width, canvas_height).unwrap();
    let x2 = data.x1 + (ncols as f32 * game.config.logical_tile_size);
    let y2 = data.y1 + (nrows as f32 * game.config.logical_tile_size);
    let mut map = Map {
      region: Region {
        p1: Point{
          x: data.x1, y: data.y1,
        },
        p2: Point{
          x: x2, y: y2,
        }
      },
      texture
    };

    // Now we pre-render the map
    for i in 0..nrows {
      if data.background_layer[i].len() != ncols {
        panic!(format!("Error in loading map {}: rows have different lengths", map_name));
      }
      for j in 0..ncols {
        for sprite in data.background_layer[i][j]
          .split(";")
          .into_iter()
          .map(|ind_str| ind_str.parse::<u32>())
          .map(|result| result.expect(&format!("Could not parse tile index")))
          .map(|ind| sprite_map.get(&ind))
          .map(|result| result.expect(&format!("Could not find tile index"))) {

          map.add_tile(&mut game.canvas, &game.config, &game.texture_manager, sprite, j as u32, i as u32)
            .expect(&format!("Something went wrong while drawing tile {}, {}", i, j));
        }
      }
    }
    map
  }

  fn add_tile(
      &mut self,
      canvas: &mut Canvas<Window>,
      config: &Config,
      texture_manager: &TextureManager,
      sprite: &Sprite,
      x_index: u32,
      y_index: u32
    ) -> Result<(), String>{

    let x_px = (x_index * config.physical_tile_size) as i32;
    let y_px = (y_index * config.physical_tile_size) as i32;
    let source_region = sprite.region;
    let target_region = Rect::new(
      x_px, y_px,
      source_region.width(),
      source_region.height());

    canvas.with_texture_canvas(
      &mut self.texture,
      |c: &mut Canvas<Window>| {
        c.copy(
          texture_manager.get_sprite_texture(sprite.texture_index),
          source_region,
          target_region
        ).unwrap();
      }
    ).unwrap();
    Ok(())
  }

  pub fn render(
      &self,
      canvas: &mut Canvas<Window>,
      config: &Config,
      target: Region) -> Result<(), String>{

    // Check that displayed screen is in bounds
    if (target.p2.x <= self.region.p1.x) | (target.p2.y <= self.region.p1.y) {
      return Ok(())
    }
    if (target.p1.x >= self.region.p2.x) | (target.p1.y >= self.region.p2.y) {
      return Ok(())
    }

    let mut width = canvas.viewport().width();
    let mut height = canvas.viewport().height();
    let mut src_offset_1 = (0, 0);
    let mut dst_offset_1 = (0, 0);
    if self.region.p1.x > target.p1.x {
      dst_offset_1.0 = ((self.region.p1.x - target.p1.x) * config.pixels_per_unit) as u32;
      width -= dst_offset_1.0;
    } else {
      src_offset_1.0 = ((target.p1.x - self.region.p1.x) * config.pixels_per_unit) as i32;
    }
    if self.region.p2.x < target.p2.x {
      width -= ((target.p2.x - self.region.p2.x) * config.pixels_per_unit) as u32;
    }

    if self.region.p1.y > target.p1.y {
      dst_offset_1.1 = ((self.region.p1.y - target.p1.y) * config.pixels_per_unit) as u32;
      height -= dst_offset_1.1;
    } else {
      src_offset_1.1 = ((target.p1.y - self.region.p1.y) * config.pixels_per_unit) as i32;
    }
    if self.region.p2.y < target.p2.y {
      height -= ((target.p2.y - self.region.p2.y) * config.pixels_per_unit) as u32;
    }

    canvas.copy(
      &self.texture,
      Rect::new(src_offset_1.0 as i32, src_offset_1.1 as i32, width, height),
      Rect::new(dst_offset_1.0 as i32, dst_offset_1.1 as i32, width, height),
    )
  }
}

impl MapData {
  fn load_map_data(map_name: &str) -> Result<MapData, String> {
    let mut path = PathBuf::new();
    path.push(".");
    path.push("assets");
    path.push("levels");
    path.push(map_name);
    path.push("map.json");
    let contents = std::fs::read_to_string(path)
      .map_err(|e| e.to_string())?;

    println!("Contents: {}", contents);

    // Attempt to load it as JSON
    let result: MapData = serde_json::from_str(&contents)
      .map_err(|e| e.to_string())?;
    Ok(result)
  }
  fn get_sprite_map(&self, expected_tile_size: u32, texture_manager: &TextureManager) -> Result<HashMap<u32, Sprite>, String> {
    let mut map: HashMap<u32, Sprite> = HashMap::new();

    // Make a map from the local tile id alias to the corresponding Sprite objects
    for (id, string) in self.tile_key.iter() {
      match texture_manager.get_sprite(string) {
        Ok(sprite) => map.insert(*id, sprite),
        Err(_) => return Err(String::from("map tile key contains invalid tile id: ") + string)
      };
    }

    // Check that all tiles are the same size and the same size as given in the config.
    for (id, sprite) in map.iter() {
      if sprite.region.width() != expected_tile_size {
        return Err(format!(
          "error with tile id {}: wrong width: {}",
          id, sprite.region.width()));
      }
      if sprite.region.height() != expected_tile_size {
        return Err(format!(
          "error with tile id {}: wrong width: {}",
          id, sprite.region.width()));
      }
    }

    Ok(map)
  }
}
