use sdl2::rect::Rect;

impl Config {
  pub fn new() -> Config {
    Config {
      logical_tile_size: 1.0,
      pixels_per_unit: 32.0,
      physical_tile_size: 32
    }
  }
}

impl Scene {
  pub fn new(cx: f32, cy: f32) -> Scene {
    Scene {cx, cy}
  }
}

impl Map {
  pub fn new(x1: f32, y1: f32, num_x: u32, num_y: u32) -> Map {
    let x2 = x1 + 1.0 * (num_x as f32);
    let y2 = y1 + 1.0 * (num_y as f32);
    Map{x1, y1, x2, y2, num_x, num_y}
  }
}

pub struct Config {
  pub logical_tile_size: f32,
  pub pixels_per_unit: f32,
  pub physical_tile_size: u32,
}
pub struct Map {
  pub x1: f32,
  pub y1: f32,
  pub x2: f32,
  pub y2: f32,
  pub num_x: u32,
  pub num_y: u32,
}
pub struct Scene {
  pub cx: f32,
  pub cy: f32
}
pub fn get_bounds_2(
    config: Config, map: Map, scene: Scene, viewport: Rect) -> (Rect, Rect) {

  let logical_cx = scene.cx;
  let logical_cy = scene.cy;
  let logical_width = (viewport.width() as f32)/(config.physical_tile_size as f32);
  let logical_height = (viewport.height() as f32)/(config.physical_tile_size as f32);
  let logical_x1 = logical_cx - logical_width/2.0;
  let logical_y1 = logical_cy - logical_height/2.0;
  let logical_x2 = logical_x1 + logical_width;
  let logical_y2 = logical_y1 + logical_height;

  let src_1 = (map.x1, map.y1);
  let dst_1 = (logical_x1, logical_y1);
  let src_2 = (map.x2, map.y2);
  let dst_2 = (logical_x2, logical_y2);

  let mut src_offset_1 = (0, 0);
  let mut dst_offset_1 = (0, 0);
  let mut width = viewport.width();
  let mut height = viewport.height();
  if src_1.0 > dst_1.0 {
    dst_offset_1.0 = ((src_1.0 - dst_1.0) * config.pixels_per_unit) as u32;
    width -= dst_offset_1.0;
  } else {
    src_offset_1.0 = ((dst_1.0 - src_1.0) * config.pixels_per_unit) as i32;
  }
  if src_2.0 < dst_2.0 {
    width -= ((dst_2.0 - src_2.0) * config.pixels_per_unit) as u32;
  }

  if src_1.1 > dst_1.1 {
    dst_offset_1.1 = ((src_1.1 - dst_1.1) * config.pixels_per_unit) as u32;
    height -= dst_offset_1.1;
  } else {
    src_offset_1.1 = ((dst_1.1 - src_1.1) * config.pixels_per_unit) as i32;
  }
  if src_2.1 < dst_2.1 {
    height -= ((dst_2.1 - src_2.1) * config.pixels_per_unit) as u32;
  }

  (
    Rect::new(src_offset_1.0 as i32, src_offset_1.1 as i32, width, height),
    Rect::new(dst_offset_1.0 as i32, dst_offset_1.1 as i32, width, height),
  )
}
pub fn get_bounds(
    config: Config, map: Map, scene: Scene, viewport: Rect) -> (Rect, Rect) {

  let logical_cx = scene.cx;
  let logical_cy = scene.cy;
  let logical_width = (viewport.width() as f32)/(config.physical_tile_size as f32);
  let logical_height = (viewport.height() as f32)/(config.physical_tile_size as f32);
  let logical_x1 = logical_cx - logical_width/2.0;
  let logical_y1 = logical_cy - logical_height/2.0;
  let logical_x2 = logical_x1 + logical_width;
  let logical_y2 = logical_y1 + logical_height;

  let src_1 = (map.x1, map.y1);
  let dst_1 = (logical_x1, logical_y1);
  let src_2 = (map.x2, map.y2);
  let dst_2 = (logical_x2, logical_y2);

  let mut src_offset_1 = (0, 0);
  let mut dst_offset_1 = (0, 0);
  let mut width = viewport.width();
  let mut height = viewport.height();
  if src_1.0 > dst_1.0 {
    dst_offset_1.0 = ((src_1.0 - dst_1.0) * config.pixels_per_unit) as u32;
    width -= dst_offset_1.0;
  } else {
    src_offset_1.0 = ((dst_1.0 - src_1.0) * config.pixels_per_unit) as i32;
  }
  if src_2.0 < dst_2.0 {
    width -= ((dst_2.0 - src_2.0) * config.pixels_per_unit) as u32;
  }

  if src_1.1 > dst_1.1 {
    dst_offset_1.1 = ((src_1.1 - dst_1.1) * config.pixels_per_unit) as u32;
    height -= dst_offset_1.1;
  } else {
    src_offset_1.1 = ((dst_1.1 - src_1.1) * config.pixels_per_unit) as i32;
  }
  if src_2.1 < dst_2.1 {
    height -= ((dst_2.1 - src_2.1) * config.pixels_per_unit) as u32;
  }

  (
    Rect::new(src_offset_1.0 as i32, src_offset_1.1 as i32, width, height),
    Rect::new(dst_offset_1.0 as i32, dst_offset_1.1 as i32, width, height),
  )
}
#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_full_full() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(4.0, 4.0);
    let map = Map::new(0.0, 0.0, 8, 8);
    let exp_src = Rect::new(0, 0, 32*8, 32*8);
    let exp_dst = Rect::new(0, 0, 32*8, 32*8);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_subset_src_full_dst() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(6.0, 6.0);
    let map = Map::new(0.0, 0.0, 12, 12);
    let exp_src = Rect::new(2*32, 2*32, 32*8, 32*8);
    let exp_dst = Rect::new(0, 0, 32*8, 32*8);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_superset_src_full_dst() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(4.0, 4.0);
    let map = Map::new(2.0, 2.0, 4, 4);
    let exp_src = Rect::new(0, 0, 32*4, 32*4);
    let exp_dst = Rect::new(2*32, 2*32, 32*4, 32*4);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_undershot_src_full_dst() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(2.0, 2.0);
    let map = Map::new(0.0, 0.0, 8, 8);

    let exp_src = Rect::new(0, 0, 32*6, 32*6);
    let exp_dst = Rect::new(2*32, 2*32, 32*6, 32*6);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_overshot_src_full_dst() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(6.0, 6.0);
    let map = Map::new(0.0, 0.0, 8, 8);

    let exp_src = Rect::new(2*32, 2*32, 32*6, 32*6);
    let exp_dst = Rect::new(0, 0, 32*6, 32*6);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_negative_full_full() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(0.0, 0.0);
    let map = Map::new(-4.0, -4.0, 8, 8);
    let exp_src = Rect::new(0, 0, 32*8, 32*8);
    let exp_dst = Rect::new(0, 0, 32*8, 32*8);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_negative_subset_src_full_dst() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(0.0, 0.0);
    let map = Map::new(-6.0, -6.0, 12, 12);
    let exp_src = Rect::new(2*32, 2*32, 32*8, 32*8);
    let exp_dst = Rect::new(0, 0, 32*8, 32*8);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_negative_superset_src_full_dst() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(0.0, 0.0);
    let map = Map::new(-2.0, -2.0, 4, 4);
    let exp_src = Rect::new(0, 0, 32*4, 32*4);
    let exp_dst = Rect::new(2*32, 2*32, 32*4, 32*4);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_negative_undershot_src_full_dst() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(-2.0, -2.0);
    let map = Map::new(-4.0, -4.0, 8, 8);

    let exp_src = Rect::new(0, 0, 32*6, 32*6);
    let exp_dst = Rect::new(2*32, 2*32, 32*6, 32*6);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

  #[test]
  fn test_negative_overshot_src_full_dst() {
    let config = Config::new();
    let viewport = Rect::new(0, 0, 32*8, 32*8);
    let scene = Scene::new(2.0, 2.0);
    let map = Map::new(-4.0, -4.0, 8, 8);

    let exp_src = Rect::new(2*32, 2*32, 32*6, 32*6);
    let exp_dst = Rect::new(0, 0, 32*6, 32*6);

    let result = get_bounds(config, map, scene, viewport);
    assert_eq!(exp_src, result.0);
    assert_eq!(exp_dst, result.1);
  }

}

