use specs::prelude::*;
use sdl2::rect::Rect;
use crate::components::*;

#[derive(Clone)]
pub struct Sprite {
  pub texture_index: usize,
  pub region: Rect
}

#[derive(Clone)]
pub struct SimpleAnimation {
  frame: usize,
  pub frames: Vec<Sprite>
}

#[derive(Clone)]
pub struct DirectionalAnimation {
  frame: usize,
  pub frames: Vec<[Sprite; 4]>
}

#[derive(Clone)]
pub enum Animation {
  Simple(SimpleAnimation),
  Directional(DirectionalAnimation)
}

impl Component for Sprite {
  type Storage = VecStorage<Self>;
}

impl Component for Animation {
  type Storage = VecStorage<Self>;
}

impl SimpleAnimation {
  pub fn new(frames: Vec<Sprite>) -> SimpleAnimation {
    SimpleAnimation {frames, frame: 0}
  }
  pub fn len(&self) -> usize {
    self.frames.len()
  }
  pub fn get_sprite(&self) -> Sprite {
    self.frames[self.frame].clone()
  }
  pub fn update(&mut self) {
    self.frame = (self.frame + 1) % self.frames.len();
  }
  pub fn set_frame(&mut self, frame: usize) {
    self.frame = frame % self.frames.len();
  }
}

impl DirectionalAnimation {
  pub fn new(up: &SimpleAnimation, down: &SimpleAnimation, left: &SimpleAnimation, right: &SimpleAnimation) -> Result<DirectionalAnimation, String> {
    let len = up.len();

    // Check all directions have same length
    for ani in [up, down, left, right].iter() {
      if ani.len() != len {
        return Err(String::from("Directional animation frames have different lengths"));
      }
    }

    let mut frames = Vec::new();
    for i in 0..len {
      frames.push([
        up.frames[i].clone(),
        down.frames[i].clone(),
        left.frames[i].clone(),
        right.frames[i].clone()
      ])
    }

    Ok(DirectionalAnimation { frame: 0, frames})
  }
  pub fn len(&self) -> usize {
    self.frames.len()
  }
  pub fn update(&mut self) {
    self.frame = (self.frame + 1) % self.frames.len();
  }
  pub fn get_sprite(&self, direction: Direction) -> Sprite {
    self.frames[self.frame][direction as usize].clone()
  }
  pub fn set_frame(&mut self, frame: usize) {
    self.frame = frame % self.frames.len();
  }
}

impl Animation {
  /// Updates state of animation by one frame.
  pub fn update(&mut self) {
    match self {
      Animation::Simple(s) => s.update(),
      Animation::Directional(s) => s.update()
    }
  }
  pub fn get_sprite(&self, direction: Direction) -> Sprite {
    match self {
      Animation::Simple(s) => s.get_sprite(),
      Animation::Directional(s) => s.get_sprite(direction)
    }
  }
  pub fn len(&self) -> usize {
    match self {
      Animation::Simple(s) => s.frames.len(),
      Animation::Directional(s) => s.frames.len()
    }
  }
  pub fn set_frame(&mut self, frame: usize) {
    match self {
      Animation::Simple(s) => s.set_frame(frame),
      Animation::Directional(s) => s.set_frame(frame)
    }
  }
}
