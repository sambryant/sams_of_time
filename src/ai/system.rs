use rand::rngs::StdRng;
use rand::thread_rng;
use rand::SeedableRng;
use specs::prelude::*;

use crate::components::*;

pub struct AiSystem {
  rng: StdRng
}

impl AiSystem {
  pub fn new() -> AiSystem {
    AiSystem {
      rng: StdRng::from_rng(thread_rng())
        .expect("Something went wrong with random number generator")
    }
  }
}

impl<'a> System<'a> for AiSystem {
  type SystemData = (
    ReadExpect<'a, DeltaTime>,
    ReadExpect<'a, Player>,
    Entities<'a>,
    ReadStorage<'a, Position>,
    WriteStorage<'a, AiWrapper>,
    WriteStorage<'a, Physics>,
    WriteStorage<'a, Action>,
    WriteStorage<'a, Orientation>,
  );

  fn run(&mut self, (dt, _player, entities, position, mut aiwrapper, mut physics, mut action, mut orientation): Self::SystemData) {
    for (entity, aiwrapper, position, physics, action, orientation) in (&*entities, &mut aiwrapper, &position, &mut physics, &mut action, &mut orientation).join() {
      aiwrapper.update(&mut self.rng, *dt, entity, position, physics, action, orientation);
    }
  }
}
