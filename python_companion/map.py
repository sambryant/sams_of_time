import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

import cairo
from typing import Dict, Any, List, Optional, Tuple

from sprite import Sprite, EMPTY_KEY
from core import ParseException, LOGICAL_PIXEL_SIZE


class Map():
  x1: float
  y1: float
  tile_map: Dict[int, Sprite]
  tile_map_rev: Dict[str, int]
  tile_width: int # width of tiles in pixels
  tile_height: int # height of tiles in pixels
  background: List[List[List[int]]]
  nrows: int
  ncols: int

  def add_row(self, key: str=None, above: bool=False) -> None:
    new_element = [] if key is None else [self.tile_map_rev[key]]
    new_row = [new_element.copy() for _ in range(self.ncols)]
    if not above:
      self.background.append(new_row)
    else:
      self.background = [new_row] + self.background
      self.y1 -= (LOGICAL_PIXEL_SIZE * self.tile_height)
    self.nrows += 1
    self._check_integrity()

  def add_col(self, key: str=None, before: bool=False) -> None:
    new_element = [] if key is None else [self.tile_map_rev[key]]
    if not before:
      for i in range(self.nrows):
        self.background[i] = self.background[i] + [new_element.copy()]
    else:
      for i in range(self.nrows):
        self.background[i] = [new_element.copy()] + self.background[i]
      self.x1 -= (LOGICAL_PIXEL_SIZE * self.tile_width)
    self.ncols += 1
    self._check_integrity()

  def __init__(self, x1: float, y1: float, tile_map: Dict[int, Sprite], background: List[List[List[int]]]) -> None:
    self.x1 = x1
    self.y1 = y1
    self.tile_map = tile_map
    self.tile_map_rev = {sprite.key: id for id, sprite in self.tile_map.items()}
    self.background = background
    self.nrows = len(self.background)
    self.ncols = len(self.background[0])
    key = list(self.tile_map.keys())[0]
    self.tile_width, self.tile_height = self.tile_map[key].get_size()

    self._check_integrity()

  def get_tile_map(self) -> Dict[int, Sprite]:
    return self.tile_map

  def add_tile_to_map(self, sprite: Sprite) -> None:
    if sprite.key in self.tile_map_rev:
      raise Exception('Tile %s already in map!' % (sprite.key))
    next_id = max(self.tile_map.keys()) + 1
    self.tile_map[next_id] = sprite
    self.tile_map_rev[sprite.key] = next_id

  def get_tile_size(self) -> Tuple[int, int]:
    return self.tile_width, self.tile_height

  def get_pixel_size(self) -> Tuple[int, int]:
    return self.tile_width * self.ncols, self.tile_height * self.nrows

  def get_sprites_at(self, pos: Tuple[int, int]) -> List[Sprite]:
    xind, yind = pos[0], pos[1]
    if xind < 0 or yind < 0:
      raise Exception('Position out of bounds: %d, %d' % (xind, yind))
    elif xind >= self.ncols or yind >= self.nrows:
      raise Exception('Position out of bounds: %d, %d' % (xind, yind))
    return [self.tile_map[id] for id in self.background[yind][xind]]

  def print_raw(self):
    print(',\n'.join(
      ('[' + ', '.join(
        '%-8s' % ('[' + ', '.join(str(i) for i in entry) + ']')
        for entry in row
      ) + ']')
      for row in self.background))

  def set_sprites_at(self, pos: Tuple[int, int], sprites: List[Sprite]) -> None:
    xind, yind = pos[0], pos[1]
    if xind < 0 or yind < 0:
      raise Exception('Position out of bounds: %d, %d' % (xind, yind))
    elif xind >= self.ncols or yind >= self.nrows:
      raise Exception('Position out of bounds: %d, %d' % (xind, yind))
    for s in sprites:
      if s.key not in self.tile_map_rev:
        raise Exception('Tile not found in tile map: %s' % s.key)
    self.background[yind][xind] = [self.tile_map_rev[s.key] for s in sprites]

  def add_tile_at(self, pos: Tuple[int, int], key: str) -> None:
    xind, yind = pos[0], pos[1]
    id = self.tile_map_rev[key]
    self.background[yind][xind].append(id)

  def remove_tile_at(self, pos: Tuple[int, int], key: str) -> None:
    xind, yind = pos[0], pos[1]
    id = self.tile_map_rev[key]
    self.background[yind][xind] = [i for i in self.background[yind][xind] if i != id]

  def draw_layer(self, cr: cairo.Context, layer: int) -> bool:
    has_tile = False
    tw, th = self.get_tile_size()
    y = 0
    for row in self.background:
      x = 0
      for layers in row:
        if layer < len(layers):
          has_tile = True
          Gdk.cairo_set_source_pixbuf(cr, self.tile_map[layers[layer]].get_image(), x, y)
          cr.paint()
        x += tw
      y += th
    return has_tile

  def draw(self, cr: cairo.Context) -> None:
    layer = 0
    while self.draw_layer(cr, layer):
      layer += 1


  def _check_integrity(self) -> None:
    """Ensures map has no obvious errors in it."""

    # 1. Check that all rows in map have same length
    for i, row in enumerate(self.background):
      if len(row) != self.ncols:
        raise ParseException('Row %d has different length %d != %d' % (i, len(row), self.ncols))

    # 2. Check that all entries in map are listed in the tile_map
    for i in range(self.nrows):
      for j in range(self.ncols):
        for id in self.background[i][j]:
          if id not in self.tile_map:
            raise ParseException('Map position (i,j)=(%d, %d) contains unknown tile id %d' % (i, j, id))

    for id, sprite in self.tile_map.items():
      w, h = sprite.get_size()
      if w != self.tile_width:
        raise ParseException('Tile id %d in tile map has unexpected width %d' % (id, w))
      if h != self.tile_height:
        raise ParseException('Tile id %d in tile map has unexpected height %d' % (id, h))
