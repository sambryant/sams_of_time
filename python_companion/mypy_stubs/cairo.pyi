
class Format():
  ARGB32: 'Format'

class Context():

  def __init__(self, surf: Surface) -> None:
    pass

  def set_source_rgb(self, r: float, g: float, b: float) -> None:
    pass

  def paint(self) -> None:
    pass

  def move_to(self, x: float, y: float) -> None:
    pass

  def line_to(self, x: float, y: float) -> None:
    pass

  def stroke(self) -> None:
    pass

  def fill(self) -> None:
    pass

  def rectangle(self, x: float, y: float, width: float, height: float) -> None:
    pass

class Surface():
  pass

class ImageSurface(Surface):

  def __init__(self, fmt: Format, width: int, height: int) -> None:
    pass
