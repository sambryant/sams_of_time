import cairo
from typing import Optional, Callable, Any, List, Tuple, TypeVar, Union, Type, Sequence, Generic


# List of okay types to pass to ListStore. TODO: This is not official. I just listed some basic
# types
ListStoreTypes = Union[int, str, bool, float, GObject.Object]

_Widget = TypeVar('_Widget', bound=Gtk.Widget)

class GLib(object):

  @staticmethod
  def timeout_add(interval: int, function: Callable, *data: Any) -> int:
    """
    Calls function repeatedly until it returns false.

    interval: 1/1000 of a second
    """
    pass

  @staticmethod
  def idle_add(function: Callable, *data: Any) -> int:
    pass

  class Error(Exception):
    pass

  class Flags(object):
    pass


class GObject(object):
  SIGNAL_RUN_FIRST: GObject.SignalFlags

  class Binding(object):
    pass

  class BindingFlags(object):
    DEFAULT: GObject.BindingFlags
    BIDIRECTIONAL: GObject.BindingFlags
    SYNC_CREATE: GObject.BindingFlags
    INVERT_BOOLEAN: GObject.BindingFlags

  class GBoxed(object):
    pass

  class GEnum(object):
    pass

  class GFlags(object):
    pass

  class GInterface(object):
    pass

  class InitiallyUnowned(GObject.Object):
    pass

  class Object(object):

    def bind_property(self, source_property: str, target: GObject.Object, target_property: str, flags: GObject.BindingFlags) -> GObject.Binding:
      pass

    def connect(self, signal: str, handler: function, *data: Any) -> int:
      pass

    def connect_after(self, signal: str, handler: function, *data: Any) -> int:
      pass

    def disconnect(self, handler_id: int) -> None:
      pass

    def emit(self, signal_name: str, *data: Any) -> None:
      pass

    def set_property(self, property_name: str, value: object) -> None:
      pass

  class ParamFlags(GLib.Flags):
    READWRITE = 3

  class SignalFlags(GLib.Flags):
    RUN_FIRST = 1


class Gio(object):

  class Action(GObject.GInterface):
    pass

  class ActionMap(GObject.GInterface):

    def add_action(self, action: Gio.SimpleAction) -> None:
      pass

    def lookup_action(self, action_name: str) -> Gio.Action:
      pass

  class ActionGroup(GObject.GInterface):
    pass

  class Application(Gio.ActionMap, Gio.ActionGroup, GObject.Object):

    def do_activate(self) -> None:
      pass

    def do_startup(self) -> None:
      pass

    def do_shutdown(self) -> None:
      pass

    def do_run_mainloop(self) -> None:
      pass

    def do_run_quit_mainloop(self) -> None:
      pass

    def run(self, argv: List[str]) -> int:
      pass

  class Icon(GObject.GInterface):
    pass

  class LoadableIcon(GObject.GInterface):
    pass

  class MenuModel(GObject.Object):
    pass

  class SimpleAction(Gio.Action, GObject.Object):

    @classmethod
    def new(cls, name: str) -> Gio.SimpleAction:
      pass

    def set_enabled(self, enabled: bool) -> None:
      pass

class Gtk(object):

  STYLE_PROVIDER_PRIORITY_APPLICATION: int
  STOCK_CANCEL: str
  STOCK_SAVE: str
  STOCK_OK: str

  TreeModelFilterVisibleFunc = Callable[[Gtk.TreeModel, Gtk.TreeIter, Any], bool]

  class Actionable(GObject.GInterface):
    pass

  class Activatable(GObject.GInterface):
    pass

  class Align(GObject.GEnum):
    START: Gtk.Align
    CENTER: Gtk.Align
    FILL: Gtk.Align
    END: Gtk.Align
    BASELINE: Gtk.Align

  class Application(Gio.Application):

    def __init__(self, **kwargs: Any) -> None:
      pass

    def set_accels_for_action(self, detailed_label: str, accels: List[str]) -> None:
      pass

    def set_menubar(self, menu: Optional[Gio.MenuModel]) -> None:
      pass

  class ApplicationWindow(Gio.ActionGroup, Gio.ActionMap, Gtk.Window):
    pass

  class Bin(Gtk.Container):

    def get_child(self) -> Optional[Gtk.Widget]:
      pass

  class Box(Gtk.Orientable, Gtk.Container):

    def pack_start(self, child: Gtk.Widget, expand: bool, fill: bool, padding: int) -> None:
      pass


  class Buildable(GObject.GInterface):
    pass

  class Builder(GObject.Object):

    def connect_signals(self, obj: object) -> None:
      pass

    def get_object(self, name: str) ->  Optional[GObject.Object]:
      pass

    @classmethod
    def new_from_string(cls, string: str, length: int) -> Gtk.Builder:
      pass

  class Button(Gtk.Actionable, Gtk.Activatable, Gtk.Bin):

    def __init__(self, label: str=None) -> None:
      pass

    def set_image(self, image: Optional[Gtk.Widget]) -> None:
      pass

    def set_label(self, label: str) -> None:
      pass

  class CellEditable(GObject.GInterface):
    pass

  class CellLayout(GObject.GInterface):
    pass

  class CheckButton(Gtk.ToggleButton):
    pass

  class ComboBox(Gtk.CellEditable, Gtk.CellLayout, Gtk.Bin):

    @classmethod
    def new_with_model_and_entry(cls, model: Gtk.TreeModel) -> Gtk.ComboBox:
      pass

    @classmethod
    def new_with_entry(cls) -> Gtk.ComboBox:
      pass

    def get_active(self) -> int:
      pass

    def get_child(self) -> Gtk.Entry:
      pass

    def set_model(self, model: Optional[Gtk.TreeModel]) -> None:
      pass

    def set_active(self, index: int) -> None:
      pass

    def set_entry_text_column(self, text_column: int) -> None:
      pass

  class Container(Gtk.Widget):

    def add(self, widget: Gtk.Widget) -> None:
      pass

    def remove(self, widget: Gtk.Widget) -> None:
      pass

  class CssProvider(Gtk.StyleProvider):

    def __init__(self) -> None:
      pass

    def load_from_data(self, data: bytes) -> bool:
      pass

  class Dialog(Gtk.Window):

    def __init__(self, title: str=None, parent: Gtk.Widget=None) -> None:
      pass

    def add_button(self, button_text: str, response_id: Gtk.ResponseType) -> Gtk.Button:
      pass

    def get_content_area(self) -> Gtk.Box:
      pass

    def run(self) -> int:
      pass

  class Editable(GObject.GInterface):
    pass

  class Entry(Gtk.CellEditable, Gtk.Editable, Gtk.Widget):

    def get_text(self) -> str:
      pass

    def set_completion(self, completion: Gtk.EntryCompletion=None) -> None:
      pass

    def set_placeholder_text(self, text: Optional[str]) -> None:
      pass

    def set_text(self, text: str) -> None:
      pass

    def set_width_chars(self, n_chars: int) -> None:
      pass

  class EntryCompletion(Gtk.Buildable, Gtk.CellLayout, GObject.Object):

    def set_model(self, model: Optional[Gtk.TreeModel]) -> None:
      pass

    def set_text_column(self, column: int) -> None:
      pass

  class EventBox(Gtk.Bin):
    pass

  class FileChooser(GObject.GInterface):

    def set_filename(self, filename: str) -> bool:
      pass

    def set_action(self, action: Gtk.FileChooserAction) -> None:
      pass

    def get_filename(self) -> Optional[str]:
      pass

    def set_do_overwrite_confirmation(self, do_overwrite_confirmation: bool) -> None:
      pass

  class FileChooserAction(GObject.GEnum):
    OPEN: Gtk.FileChooserAction
    SAVE: Gtk.FileChooserAction
    SELECT_FOLDER: Gtk.FileChooserAction
    CREATE_FOLDER: Gtk.FileChooserAction

  class FileChooserButton(Gtk.Box, Gtk.FileChooser):
    pass

  class FileChooserDialog(Gtk.Dialog, Gtk.FileChooser):

    def set_action(self, action: Gtk.FileChooserAction) -> None:
      pass

  class Grid(Gtk.Orientable, Gtk.Container):

    def attach(self, child: Gtk.Widget, left: int, top: int, width: int, height: int) -> None:
      pass

    def set_column_spacing(self, spacing: int) -> None:
      pass

  class HeaderBar(Gtk.Container):

    def get_title(self) -> Optional[str]:
      pass

    def set_title(self, title: Optional[str]) -> None:
      pass

    def get_subtitle(self) -> Optional[str]:
      pass

    def set_subtitle(self, title: Optional[str]) -> None:
      pass

  class IconTheme(GObject.Object):

    @classmethod
    def get_default(cls) -> Gtk.IconTheme:
      pass

    def load_icon(self, icon_name: str, size: int, flags: int) -> Optional[GdkPixbuf.Pixbuf]:
      pass

  class Image(Gtk.Misc):

    def clear(self) -> None:
      pass

    @classmethod
    def new_from_pixbuf(cls, pixbuf: Optional[GdkPixbuf.Pixbuf]) -> Gtk.Image:
      pass

    def set_from_pixbuf(self, pixbuf: Optional[GdkPixbuf.Pixbuf]) -> None:
      pass


  class Label(Gtk.Misc):

    def __init__(self, label: str=None) -> None:
      pass

    def set_text(self, label: str) -> None:
      pass

  class ListStore(Gtk.Buildable, Gtk.TreeDragDest, Gtk.TreeDragSource, Gtk.TreeModel, Gtk.TreeSortable, GObject.Object):

    def __init__(self, *data: Type[ListStoreTypes]) -> None:
      pass

    def append(self, row: List[object]=None) -> Gtk.TreeIter:
      pass

    def clear(self) -> None:
      pass

    def remove(self, iter: Gtk.TreeIter) -> bool:
      pass

  class Menu(Gtk.MenuShell):

    def attach_to_widget(self, attach: Gtk.Widget) -> None:
      pass

    def popup(
      self,
      parent_menu_shell: Optional[Gtk.Widget],
      parent_menu_item: Optional[Gtk.Widget],
      func: None, # NIY
      data: None, # NIY
      button: int,
      activate_time: int) -> None:
      pass

  class MenuItem(Gtk.Actionable, Gtk.Activatable, Gtk.Bin):

    @classmethod
    def new_with_label(cls, label: str) -> Gtk.MenuItem:
      pass

    def set_submenu(self, menu: Optional[Gtk.Menu]) -> None:
      pass

  class MenuShell(Gtk.Container):

    def append(self, child: Gtk.MenuItem) -> None:
      pass

  class Misc(Gtk.Widget):
    pass

  class Orientable(GObject.GInterface):

    def set_orientation(self, o: Gtk.Orientation) -> None:
      pass

    def get_orientation(self) -> Gtk.Orientation:
      pass

  class Orientation(GObject.GEnum):
    HORIZONTAL: Gtk.Orientation
    VERTICAL: Gtk.Orientation

  class Overlay(Gtk.Bin):

    def add_overlay(self, widget: Gtk.Widget) -> None:
      pass

  class Paned(Gtk.Orientable, Gtk.Container):

    def get_child1(self) -> Optional[Gtk.Widget]:
      pass

    def get_child2(self) -> Optional[Gtk.Widget]:
      pass

    def pack1(self, child: Gtk.Widget, resize: bool, shrink: bool) -> None:
      pass

    def pack2(self, child: Gtk.Widget, resize: bool, shrink: bool) -> None:
      pass

  class Requisition(object):
    width: int
    height: int
    pass

  class ResponseType(GObject.GEnum):
    NONE: Gtk.ResponseType
    APPLY: Gtk.ResponseType
    HELP: Gtk.ResponseType
    REJECT: Gtk.ResponseType
    ACCEPT: Gtk.ResponseType
    DELETE_EVENT: Gtk.ResponseType
    OK: Gtk.ResponseType
    CANCEL: Gtk.ResponseType
    CLOSE: Gtk.ResponseType
    YES: Gtk.ResponseType
    NO: Gtk.ResponseType

  class Scrollable(GObject.GInterface):
    pass

  class ScrolledWindow(Gtk.Bin):
    pass

  class StyleContext(GObject.Object):

    def add_class(self, class_name: str) -> None:
      pass

    @classmethod
    def add_provider_for_screen(cls, screen: Gdk.Screen, provider: Gtk.StyleProvider, priority: int) -> None:
      pass

  class StyleProvider(GObject.GInterface):
    pass

  class TextBuffer(GObject.Object):

    def get_end_iter(self) -> Gtk.TextIter:
      pass

    def get_start_iter(self) -> Gtk.TextIter:
      pass

    def get_text(self, start: Gtk.TextIter, end: Gtk.TextIter, include_hidden_chars: bool) -> str:
      pass

    def set_text(self, text: str, length: int=-1) -> None:
      pass

  class TextIter(object):
    pass


  class TextView(Gtk.Scrollable, Gtk.Container):

    def get_buffer(self) -> Gtk.TextBuffer:
      pass

  class ToggleButton(Gtk.Button):

    def get_active(self) -> bool:
      pass

    def set_active(self, is_active: bool) -> None:
      pass

  class TreeDragDest(GObject.GInterface):
    pass

  class TreeDragSource(GObject.GInterface):
    pass

  class TreeIter(object):
    pass

  class TreeModel(GObject.GInterface):


    def __len__(self) -> int:
      pass

    def __getitem__(self, key: Union[Gtk.TreeIter, Gtk.TreePath, int, str]) -> Gtk.TreeModelRow:
      """Typedefs above are non-official"""
      pass

    def __setitem__(self, key: Gtk.TreeIter, value: List[object]) -> None:
      """Typedefs above are non-official"""
      pass

    def __iter__(self) -> Gtk.TreeModelRowIter:
      pass

    def filter_new(self, root: Gtk.TreePath=None) -> Gtk.TreeModelFilter:
      pass

    def get_iter(self, path: Gtk.TreePath) -> Gtk.TreeIter:
      pass

    def get_iter_from_string(self, path_str: str) -> Gtk.TreeIter:
      pass

    def get_path(self, iter: Gtk.TreeIter) -> Gtk.TreePath:
      pass

    def iter_children(self, iter: Optional[Gtk.TreeIter]) -> Optional[Gtk.TreeIter]:
      pass

    def iter_next(self, iter: Gtk.TreeIter) -> Optional[Gtk.TreeIter]:
      pass

    def iter_parent(self, child: Gtk.TreeIter) -> Optional[Gtk.TreeIter]:
      pass

    def iter_previous(self, iter: Gtk.TreeIter) -> Optional[Gtk.TreeIter]:
      pass

    def sort_new_with_model(self) -> Gtk.TreeModelSort:
      pass

  class TreeModelRow(GObject.GBoxed):

    def __getitem__(self, key: int) -> ListStoreTypes:
      """Typedefs above are non-official"""
      pass

    def __setitem__(self, key: int, value: ListStoreTypes) -> None:
      """Typedefs above are non-official"""
      pass

    def get_next(self) -> Optional[Gtk.TreeModelRow]:
      pass

    def get_parent(self) -> Optional[Gtk.TreeModelRow]:
      pass

    def get_previous(self) -> Optional[Gtk.TreeModelRow]:
      pass

    def iterchildren(self) -> Gtk.TreeModelRowIter:
      pass

  class TreeModelRowIter(object):

    def __next__(self) -> Gtk.TreeModelRow:
      pass

    def next(self) -> Gtk.TreeModelRow:
      pass

  class TreeModelSort(Gtk.TreeDragSource, Gtk.TreeModel, Gtk.TreeSortable, GObject.Object):
    pass

  class TreeModelFilter(Gtk.TreeDragSource, Gtk.TreeModel, GObject.Object):

    def refilter(self) -> None:
      pass

    def set_visible_func(self, func: Gtk.TreeModelFilterVisibleFunc, *data: Any) -> None:
      pass

  class TreePath(object):

    @classmethod
    def new_from_string(cls, path: str) -> Optional[Gtk.TreePath]:
      pass

    def copy(self) -> Gtk.TreePath:
      pass

    def get_depth(self) -> int:
      pass

    def up(self) -> None:
      pass

  class TreeSelection(GObject.Object):

    def get_selected(self) -> Tuple[Gtk.TreeModel, Optional[Gtk.TreeIter]]:
      pass

    def get_selected_rows(self) -> Tuple[Gtk.TreeModel, List[Gtk.TreePath]]:
      pass

    def set_select_function(self, func: Callable[[Gtk.TreeSelection, Gtk.TreeModel, Gtk.TreePath, bool], bool]) -> None:
      pass

    def select_path(self, path: Gtk.TreePath) -> None:
      pass

    def unselect_all(self) -> None:
      pass

  class TreeSortable(GObject.GInterface):
    pass

  class TreeStore(Gtk.Buildable, Gtk.TreeDragDest, Gtk.TreeDragSource, Gtk.TreeModel, Gtk.TreeSortable, GObject.Object):

    def __init__(self, *data: Type[ListStoreTypes]) -> None:
      pass

    def append(self, parent: Optional[Gtk.TreeIter], row: object=None) -> Gtk.TreeIter:
      pass

    def clear(self) -> None:
      pass

    def remove(self, iter: Gtk.TreeIter) -> bool:
      pass

  class TreeView(Gtk.Scrollable, Gtk.Container):

    def expand_row(self, path: Gtk.TreePath, open_all: bool) -> bool:
      pass

    def get_column(self, n: int) -> Optional[Gtk.TreeViewColumn]:
      pass

    def get_model(self) -> Optional[Gtk.TreeModel]:
      pass

    def get_selection(self) -> Gtk.TreeSelection:
      pass

    def set_cursor(self, path: Gtk.TreePath, focus_column: Optional[Gtk.TreeViewColumn], start_editing: bool) -> None:
      pass

    def set_model(self, model: Optional[Gtk.TreeModel]) -> None:
      pass

  class TreeViewColumn(Gtk.Buildable, Gtk.CellLayout, GObject.InitiallyUnowned):
    pass

  class Widget(GObject.InitiallyUnowned, Gtk.Buildable):

    def destroy(self) -> None:
      pass

    def do_get_preferred_height(self) -> Tuple[int, int]:
      pass

    def do_get_preferred_width(self) -> Tuple[int, int]:
      pass

    def get_allocated_height(self) -> int:
      pass

    def get_allocated_width(self) -> int:
      pass

    def get_ancestor(self, widget_type: Type[_Widget]) -> Optional[_Widget]:
      pass

    def get_preferred_width(self) -> Tuple[int, int]:
      pass

    def get_preferred_size(self) -> Tuple[Gtk.Requisition, Gtk.Requisition]:
      pass

    def get_size_request(self) -> Tuple[int, int]:
      pass

    def get_style_context(self) -> Gtk.StyleContext:
      pass

    def get_toplevel(self) -> Gtk.Widget:
      pass

    def grab_focus(self) -> None:
      pass

    def set_sensitive(self, sensitive: bool) -> None:
      pass

    def set_size_request(self, width: int, height: int) -> None:
      pass

    def hide(self) -> None:
      pass

    def show(self) -> None:
      pass

    def show_all(self) -> None:
      pass

  class Window(Gtk.Bin):

    def present(self) -> None:
      pass

    def set_default_size(self, width: int, height: int) -> None:
      pass

    def set_application(self, app: Optional[Gtk.Application]) -> None:
      pass

    def set_transient_for(self, parent: Optional[Gtk.Window]) -> None:
      pass

  class WrapMode(GObject.GEnum):
    NONE: Gtk.WrapMode
    CHAR: Gtk.WrapMode
    WORD: Gtk.WrapMode
    WORD_CHAR: Gtk.WrapMode

  # The following classes are all related to Template behavior. They are not documented on usual GTK
  # python documentation site, but the source code is here:
  # https://gitlab.gnome.org/GNOME/pygobject/blob/master/gi/_gtktemplate.py
  class Callback(object):

    def __init__(self, name: str=None):
      pass

    def __call__(self, func: Callable) -> Callable:
      pass

  class Child(object):
    pass

  class Template(object):

    Callback = Gtk.Callback
    Child = Gtk.Child

    @classmethod
    def from_file(cls, filename: str) -> Gtk.Template:
      pass

    @classmethod
    def from_string(cls, string: str) -> Gtk.Template:
      pass

    @classmethod
    def from_resource(cls, resource_path: str) -> Gtk.Template:
      pass

    def __call__(self, cls: type) -> type:
      pass


class Gdk(object):
  BUTTON_SECONDARY: int

  @staticmethod
  def cairo_set_source_pixbuf(cr: cairo.Context, pb: GdkPixbuf.Pixbuf, x: float, y: float) -> None:
    pass

  @staticmethod
  def cairo_set_source_rgba(cr: cairo.Context, rgba: Gdk.RGBA) -> None:
    pass

  @staticmethod
  def pixbuf_get_from_surface(surface: cairo.Surface, src_x: int, src_y: int, width: int, height: int) -> Optional[GdkPixbuf.Pixbuf]:
    pass

  class Display(GObject.Object):

    @classmethod
    def get_default(cls) -> Gdk.Display:
      pass

    def get_monitor(self, num: int) -> Optional[Gdk.Monitor]:
      pass

    def get_primary_monitor(self) -> Optional[Gdk.Monitor]:
      pass

  class EventButton(object):
    button: int
    x: float
    y: float
    type: Gdk.EventType

    def get_time(self) -> int:
      pass

  class EventKey(object):
    group: int
    hardware_keycode: int
    is_modifier: int
    keyval: int
    length: int
    send_event: int
    state: Gdk.ModifierType
    string: str
    time: int
    type: Gdk.EventType
    window: Gdk.Window

  class EventType(GObject.GEnum):
    BUTTON_PRESS = 4

  class ModifierType(GObject.GFlags):
    pass

  class Monitor(GObject.Object):

    def get_workarea(self) -> Gdk.Rectangle:
      pass

  class Rectangle(object):
    height: int
    width: int
    x: int
    y: int
    pass

  class RGBA():
    def __init__(Self, r: float, g: float, b: float, a: float) -> None:
      pass

  class Screen(GObject.Object):

    @classmethod
    def get_default(cls) -> Gdk.Screen:
      pass

  class Window(GObject.Object):
    pass


class GdkPixbuf(object):

  class ColorSpace(GObject.GEnum):
    pass

  class InterpType(object):

    BILINEAR: GdkPixbuf.InterpType

  class Pixbuf(GObject.Object, Gio.Icon, Gio.LoadableIcon):

    @classmethod
    def new(cls, colorspace: GdkPixbuf.ColorSpace, has_alpha: bool, bits_per_sample: int, width: int, height: int) -> Optional[GdkPixbuf.Pixbuf]:
      pass

    @classmethod
    def new_from_file_at_scale(cls, filename: str, width: int, height: int, preserve_aspect_ratio: bool) -> GdkPixbuf.Pixbuf:
      pass

    @classmethod
    def new_from_file(cls, filename: str) -> GdkPixbuf.Pixbuf:
      pass

    def copy_area(self, src_x: int, src_y: int, width: int, height: int, dest_pixbuf: GdkPixbuf.Pixbuf, dest_x: int, dest_y: int) -> None:
      pass

    def new_subpixbuf(self, src_x: int, src_y: int, width: int, height: int) -> GdkPixbuf.Pixbuf:
      pass

    def get_width(self) -> int:
      pass

    def get_height(self) -> int:
      pass

    def get_colorspace(self) -> GdkPixbuf.ColorSpace:
      pass

    def get_has_alpha(self) -> bool:
      pass

    def get_bits_per_sample(self) -> int:
      pass

    def scale(self, dest: GdkPixbuf.Pixbuf, dest_x: int, dest_y: int, width: int, height: int, offset_x: float, offset_y: float, scale_x: float, scale_y: float, interp_type: GdkPixbuf.InterpType) -> None:
      pass

class WebKit2(object):

  class WebViewBase(Gtk.Container):
    pass

  class WebView(WebViewBase):

    def get_title(self) -> str:
      pass

    def load_html(self, content: str, base_uri: str=None) -> None:
      pass
