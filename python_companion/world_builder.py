import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
import cairo

import argparse
import enum
import functools
import pkgutil
import sys
from typing import Any, List, Optional, Dict, Set, TypeVar, Type, Callable, Tuple, cast

from sprite_manager import SpriteManager
from animation_manager import AnimationManager
from map_manager import MapManager
from map import Map
from sprite import Sprite
from animation import Animation
from tile_viewer import TileViewer
from tile_chooser import TileChooser

class Mode(enum.Enum):
  Select = 0
  Append = 1
  Erase = 2


@Gtk.Template.from_file('world-builder.ui')
class WorldBuilder(Gtk.ApplicationWindow):
  __gtype_name__ = 'WorldBuilder'
  main_paned = cast(Gtk.Paned, Gtk.Template.Child())
  content_paned = cast(Gtk.Paned, Gtk.Template.Child())
  map_image = cast(Gtk.Image, Gtk.Template.Child())
  map_list_model = cast(Gtk.ListStore, Gtk.Template.Child())
  map_list_view = cast(Gtk.TreeView, Gtk.Template.Child())
  tile_map_viewer = cast(TileViewer, Gtk.Template.Child())
  layer_tile_viewer = cast(TileViewer, Gtk.Template.Child())
  mode_button_select = cast(Gtk.Button, Gtk.Template.Child())
  mode_button_append = cast(Gtk.Button, Gtk.Template.Child())

  # Flags
  flag_draw_grid: bool
  flag_show_selected: bool

  mode: Mode
  mode_to_button: Dict[Mode, Gtk.ToggleButton]

  selected_tile: Optional[Tuple[int, int]]
  map_manager: MapManager
  sprite_manager: SpriteManager
  animation_manager: AnimationManager
  world_map: Optional[Map]
  map_pixbuf: Optional[GdkPixbuf.Pixbuf]

  def __init__(self) -> None:
    super().__init__()

    # Load CSS
    data = open('world-builder.css', 'rb').read()
    if not data:
      raise Exception('Failed to load CSS')
    style_provider = Gtk.CssProvider()
    style_provider.load_from_data(data)
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(),
        style_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

    self.flag_draw_grid = True
    self.flag_show_selected = True

    self.main_paned.set_property('position', 200)
    self.content_paned.set_property('position', 500)

    self.sprite_manager = SpriteManager.get()
    self.animation_manager = AnimationManager(self.sprite_manager)
    self.map_manager = MapManager(self.sprite_manager)
    self.world_map = None
    self.map_pixbuf = None
    self.selected_tile = None
    self._fill_map_list()
    self.mode_to_button = {
      Mode.Select: self.mode_button_select,
      Mode.Append: self.mode_button_append
    }
    self.mode = Mode.Select
    self.set_mode(Mode.Select)

  def _fill_map_list(self) -> None:
    self.map_list_model.clear()
    for name in self.map_manager.list_maps():
      self.map_list_model.append([name])

  @Gtk.Template.Callback('map-selection-changed')
  def _on_map_selection_changed(self, *_: Any) -> None:
    model, itr = self.map_list_view.get_selection().get_selected()
    if itr:
      map_name = cast(str, model[itr][0])
      self._set_map(self.map_manager.get(map_name))
    else:
      self._set_map(None)

  @Gtk.Template.Callback('tile-map-selection-changed')
  def _on_tile_map_tile_selection_changed(self, *_: Any) -> None:
    pass

  @Gtk.Template.Callback('tile-layer-selection-changed')
  def _on_tile_layer_selection_changed(self, *_: Any) -> None:
    pass

  @Gtk.Template.Callback('tile-layer-removed')
  def _on_tile_layer_removed(self, _: TileViewer, key: str) -> None:
    if not self.selected_tile:
      raise Exception('No tile selected')
    if not self.world_map:
      raise Exception('No map selected')

    self.world_map.remove_tile_at(self.selected_tile, key)
    self._set_selected_tile(self.selected_tile)

  @Gtk.Template.Callback('tile-layer-added')
  def _on_tile_layer_added(self, *_: Any) -> None:
    if not self.selected_tile:
      raise Exception('No tile selected')
    if not self.world_map:
      raise Exception('No map selected')

    avail_sprites = list(self.world_map.get_tile_map().values())
    tc = TileChooser(avail_sprites, self)
    response = tc.run()
    if response == Gtk.ResponseType.OK:
      sprite_key = tc.get_selected()
      if sprite_key:
        self.world_map.add_tile_at(self.selected_tile, sprite_key)
    self._set_selected_tile(self.selected_tile)
    tc.destroy()

  @Gtk.Template.Callback('map-image-button-press')
  def _on_map_image_clicked(self, ev_box: Gtk.EventBox, ev_but: Gdk.EventButton) -> None:
    if not self.world_map:
      return
    if ev_but.type == Gdk.EventType.BUTTON_PRESS:
      # Determine which tile click happened at
      tw, th = self.world_map.get_tile_size()
      w, h = self.world_map.get_pixel_size()
      x, y = ev_but.x, ev_but.y
      if x > w or y > h:
        return

      pos = (int(x/tw), int(y/th))

      if self.mode == Mode.Select:
        self._set_selected_tile(pos)
      elif self.mode == Mode.Append:
        key = self.tile_map_viewer.get_selected()
        if key:
          self.world_map.add_tile_at(pos, key)
          self._redraw_map()
        else:
          raise Exception('No tile selected from map')
      elif self.mode == Mode.Erase:
        self.world_map.set_sprites_at(pos, [])
        self._redraw_map()
      else:
        raise Exception('Undefined behavior')

  def _set_selected_tile(self, pos: Optional[Tuple[int, int]]) -> None:
    self.selected_tile = pos
    if self.selected_tile and self.world_map:
      self.layer_tile_viewer.set_model(self.world_map.get_sprites_at(self.selected_tile))
    else:
      self.layer_tile_viewer.set_model([])
    self._redraw_map()

  def _set_map(self, world_map: Optional[Map]) -> None:
    if world_map == self.world_map:
      return

    self.world_map = world_map
    if self.world_map:
      self.tile_map_viewer.set_model(list(self.world_map.get_tile_map().values()))
    else:
      self.tile_map_viewer.set_model([])
    self._redraw_map()

  def set_mode(self, mode: Mode, *_: Any) -> None:
    self.mode = mode

  def _redraw_map(self) -> None:
    if self.world_map:
      tw, th = self.world_map.get_tile_size()
      w, h = self.world_map.get_pixel_size()
      # self.map_pixbuf = self.world_map.get_image().copy()

      frmt = cairo.Format.ARGB32
      surf = cairo.ImageSurface(frmt, w, h)
      cr = cairo.Context(surf)

      # Draw background color
      cr.set_source_rgb(1.0, 0.0, 1.0)
      cr.rectangle(0, 0, w, h)
      cr.fill()

      # Draw world map
      self.world_map.draw(cr)

      if self.flag_draw_grid:

        # Draw grid lines
        cr.set_source_rgb(0.5, 0.5, 0.5)
        x = 0
        while x < w:
          cr.move_to(x, 0.0)
          cr.line_to(x, h)
          cr.stroke()
          x += tw
        y = 0
        while y < h:
          cr.move_to(0.0, y)
          cr.line_to(w, y)
          cr.stroke()
          y += th

      if self.flag_show_selected and self.selected_tile:
        # Draw grid lines
        Gdk.cairo_set_source_rgba(cr, Gdk.RGBA(1.0, 1.0, 0.0, 0.5))
        x, y = self.selected_tile[0]*tw, self.selected_tile[1]*th
        print('Drew rect at %d,%d (%d, %d)' % (x,y,tw, th))
        cr.rectangle(x, y, tw, th)
        cr.fill()


      self.map_image.set_from_pixbuf(Gdk.pixbuf_get_from_surface(surf, 0, 0, w, h))

      # self.map_image.set_from_pixbuf(self.world_map.get_image())
    else:
      self.map_image.set_from_pixbuf(None)

  @Gtk.Template.Callback('add-row-above')
  def _on_add_row_above(self, *_: Any) -> None:
    if not self.world_map:
      return
    else:
      self.world_map.add_row(above=True)
    self._redraw_map()

  @Gtk.Template.Callback('add-row-below')
  def _on_add_row_below(self, *_: Any) -> None:
    if not self.world_map:
      return
    else:
      self.world_map.add_row(above=False)
    self._redraw_map()

  @Gtk.Template.Callback('add-col-left')
  def _on_add_col_left(self, *_: Any) -> None:
    if not self.world_map:
      return
    else:
      self.world_map.add_col(before=True)
    self._redraw_map()

  @Gtk.Template.Callback('add-col-right')
  def _on_add_col_right(self, *_: Any) -> None:
    if not self.world_map:
      return
    else:
      self.world_map.add_col(before=False)
    self._redraw_map()


class Application(Gtk.Application):
  world_builder: WorldBuilder

  def __init__(self) -> None:
    Gtk.Application.__init__(self)
    self.world_builder = WorldBuilder()

  def do_startup(self) -> None:
    Gtk.Application.do_startup(self)
    self.world_builder.set_application(self)

    for label, method in [
      ('erase-mode', functools.partial(self.world_builder.set_mode, Mode.Erase)),
      ('select-mode', functools.partial(self.world_builder.set_mode, Mode.Select)),
      ('append-mode', functools.partial(self.world_builder.set_mode, Mode.Append)),
    ]:
      action = Gio.SimpleAction.new(label)
      #action.set_enabled(enabled)
      action.connect('activate', method)
      self.add_action(action)
      # if accel:
      #   self.set_accels_for_action('app.'+label, [accel]) # todo: why do we need app. and where does this come from

  def do_activate(self) -> None:
    self.world_builder.present()

  def run(self, argv: List[str]=[]) -> int:
    return super().run(argv)


def main() -> int:
  return Application().run()

if __name__ == '__main__':
  main()
