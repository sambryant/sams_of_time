import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
import cairo

import argparse
import pkgutil
import sys
from typing import Any, List, Optional, Dict, Set, TypeVar, Type, Callable, Tuple, cast

from sprite import Sprite
from sprite_manager import SpriteManager


@Gtk.Template.from_file('tile-viewer.ui')
class TileViewer(Gtk.Box):
  __gtype_name__ = 'TileViewer'
  __gsignals__ = {
    'selection-changed': (GObject.SignalFlags.RUN_FIRST, None, ()),
    'add-tile': (GObject.SignalFlags.RUN_FIRST, None, ()),
    'remove-tile': (GObject.SignalFlags.RUN_FIRST, None, (object,)), # type = str (tile key)
  }
  __gproperties__ = {
    'editable': (bool, 'hides/shows the add/remove buttons', 'hides/shows the add/remove buttons', True, GObject.ParamFlags.READWRITE)
  }
  add_button = cast(Gtk.Button, Gtk.Template.Child())
  remove_button = cast(Gtk.Button, Gtk.Template.Child())
  tile_model = cast(Gtk.ListStore, Gtk.Template.Child())
  tile_view = cast(Gtk.TreeView, Gtk.Template.Child())
  tile_preview = cast(Gtk.Image, Gtk.Template.Child())

  _selected_tile: Optional[str]
  _sprite_manager: SpriteManager
  _ignore_selection_change: bool

  def __init__(self) -> None:
    super().__init__()
    self._editable = True
    self._selected_tile = None
    self._sprite_manager = SpriteManager.get()
    self._ignore_selection_change = False
    self.connect('realize', self._on_realize)

  def do_get_property(self, prop):
    if prop.name == 'editable':
      return self._editable
    else:
      return super().get_property(key)

  def do_set_property(self, prop, value):
    if prop.name == 'editable':
      if not isinstance(value, bool):
        raise AttributeError('Property %s must be a bool' % prop.name)
      self._editable = value
      self.add_button.set_property('visible', value)
      self.remove_button.set_property('visible', value)
    else:
      return super().set_property(key, value)

  def get_selected(self) -> Optional[str]:
    return self._selected_tile

  def set_model(self, sprites: List[Sprite]) -> None:
    self._ignore_selection_change = True
    self.tile_view.set_model(None)
    self.tile_model.clear()
    for i, spr in enumerate(sprites):
      self.tile_model.append([i, spr.key])
    self.tile_view.set_model(self.tile_model)
    self._ignore_selection_change = False
    self._on_tile_selection_changed()

  @Gtk.Template.Callback('realize')
  def _on_realize(self, *_: Any) -> None:
    self._on_tile_selection_changed()

  @Gtk.Template.Callback('tile-selection-changed')
  def _on_tile_selection_changed(self, *_: Any):
    if self._ignore_selection_change:
      return
    model, itr = self.tile_view.get_selection().get_selected()
    if itr:
      key = cast(str, model[itr][1])
      self._selected_tile = key
      self.tile_preview.set_from_pixbuf(self._sprite_manager.get_sprite(key).get_image())
    else:
      self._selected_tile = None
      self.tile_preview.set_from_pixbuf(self._sprite_manager.get_empty().get_image())

    self.emit('selection-changed')

  @Gtk.Template.Callback('add-clicked')
  def _on_add_clicked(self, *_: Any) -> None:
    self.emit('add-tile')

  @Gtk.Template.Callback('remove-clicked')
  def _on_remove_clicked(self, *_: Any) -> None:
    self.emit('remove-tile', self._selected_tile)

