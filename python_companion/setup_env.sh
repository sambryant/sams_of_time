export _PROJECT_DIR="$PWD"

# Set MYPY dir so that mypy can see my custom stubs
export MYPYPATH="$_PROJECT_DIR/mypy_stubs"

