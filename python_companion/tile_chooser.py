import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from typing import Any, Dict, TypeVar, Type, Union, Generic, List, Optional, cast
from sprite import Sprite
from tile_viewer import TileViewer


class TileChooser(Gtk.Dialog):
  __gtype_name__ = 'TileChooser'
  tile_viewer = cast(TileViewer, Gtk.Template.Child())

  def __init__(self, sprites: List[Sprite], parent: Gtk.Widget) -> None:
    Gtk.Dialog.__init__(self, title='Choose tile')
    self.set_transient_for(parent.get_ancestor(Gtk.Window) if parent else None)
    self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
    self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
    self.tile_viewer = TileViewer()
    self.tile_viewer.set_model(sprites)
    self.get_content_area().add(self.tile_viewer)
    self.show_all()

  def get_selected(self) -> Optional[str]:
    return self.tile_viewer.get_selected()
