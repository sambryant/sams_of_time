from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

import os, json
from typing import ClassVar, Dict, Any, List, Tuple, cast

from core import ParseException, MissingResourceError
from sprite import Sprite
from loaders import ResourceLoader
from map import Map
from sprite_manager import SpriteManager

class RLevelData(ResourceLoader):
  levels: List[str]

  def __init__(self, resource_dir: str, json_dict: Dict[str, Any]) -> None:
    super().__init__(resource_dir, json_dict)
    self.levels = json_dict['levels']

  def consume(self, sprite_manager: SpriteManager, maps: Dict[str, Map]) -> None:
    super().check_consume()
    for level in self.levels:
      if level in maps:
        raise ParseException('Duplicate level name: %s' % level)

      level_dir = os.path.join(self.resource_dir, level)
      maps[level] = RData.load_resource(level_dir).consume(sprite_manager)

class RData(ResourceLoader):
  JSON_FILE: ClassVar[str] = 'map.json'
  x1: float
  y1: float
  tile_key: List[Tuple[int, str]]
  background_layer: List[List[str]]

  def __init__(self, resource_dir: str, json_dict: Dict[str, Any]) -> None:
    super().__init__(resource_dir, json_dict)
    self.x1 = json_dict['x1']
    self.y1 = json_dict['y1']
    self.tile_key = [(i, key) for i, key in json_dict['tile_key']]
    self.background_layer = json_dict['background_layer']

  def consume(self, sprite_manager: SpriteManager) -> Map:
    super().check_consume()
    try:
      # convert tile key into sprite map
      tile_map: Dict[int, Sprite] = {}
      for id, key in self.tile_key:
        if id in tile_map:
          raise ParseException('duplicate id in tile map: %d' % id)
        tile_map[id] = sprite_manager.get_sprite(key) # can raise MissingResourceError

      # convert background layer strings into list of ints for each coordinate
      background = [
        [list(map(int, entry.split(';'))) for entry in row]
        for row in self.background_layer
      ]

      m = Map(self.x1, self.y1, tile_map, background)

      del self
      return m

    except ParseException as ex:
      raise ParseException('Error in resource %s: %s' % (self, ex.msg))
    except MissingResourceError as ex:
      raise ParseException('Error in resource %s: %s' % (self, ex.msg))

if __name__ == '__main__':
  print('hi')
  sm = SpriteManager()
  RData.load_resource('../assets/levels/test1').consume(sm)
