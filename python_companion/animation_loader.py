from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

import os, json
from typing import ClassVar, Dict, Any, List, cast

from animation import Animation, RegAnimation, DirAnimation
from core import MissingResourceError, ParseException
from loaders import ResourceLoader
from sprite_manager import SpriteManager


class RData(ResourceLoader):
  animations: List[RData.RAnimation]
  movement_animations: List[RData.RDirAnimation]

  def __init__(self, resource_dir: str, json_dict: Dict[str, Any]) -> None:
    super().__init__(resource_dir, json_dict)
    self.animations = list(map(RData.RAnimation, json_dict['animations']))
    self.movement_animations = list(map(RData.RDirAnimation, json_dict['movement_animations']))

  class RAnimation():
    key: str
    sprites: List[str]

    def __init__(self, json_dict: Dict[str, Any]) -> None:
      self.key = json_dict['key']
      self.sprites = json_dict['sprites']

    def __repr__(self) -> str:
      return self.key

    def _consume(self, sprite_manager: SpriteManager, animations: Dict[str, Animation]) -> None:
      try:
        if self.key in animations:
          raise ParseException('Duplicate animation key: %s' % self.key)
        animations[self.key] = RegAnimation([
          sprite_manager.get_sprite(s) for s in self.sprites
        ])
      except ParseException as ex:
        raise ParseException('Error in animation %s: %s' % (self, ex.msg))
      except MissingResourceError as ex:
        raise ParseException('Error in animation %s: %s' % (self, ex.msg))
      del self

  class RDirAnimation():
    key: str
    up: str
    down: str
    left: str
    right: str

    def __init__(self, json_dict: Dict[str, Any]) -> None:
      self.key = json_dict['key']
      self.up = json_dict['up']
      self.down = json_dict['down']
      self.left = json_dict['left']
      self.right = json_dict['right']

    def __repr__(self) -> str:
      return self.key

    def _consume(self, animations: Dict[str, Animation]) -> None:
      try:
        if self.key in animations:
          raise ParseException('Duplicate directional animation key: %s' % self.key)

        # extracts sub-animation from already built map
        def get_sub_animation(key: str) -> RegAnimation:
          if key in animations:
            a = animations[key]
            if isinstance(a, RegAnimation):
              return a
            else:
              raise ParseException('contained another dir animation "%s"' % key)
          else:
            raise MissingResourceError('unknown animation "%s"' % key)

        animations[self.key] = DirAnimation(
          get_sub_animation(self.up).frames,
          get_sub_animation(self.down).frames,
          get_sub_animation(self.left).frames,
          get_sub_animation(self.right).frames
        )
      except ParseException as ex:
        raise ParseException('Error in dir animation %s: %s' % (self, ex.msg))
      except MissingResourceError as ex:
        raise ParseException('Error in dir animation %s: %s' % (self, ex.msg))
      del self

  def consume(self, sprite_manager: SpriteManager, animations: Dict[str, Animation]) -> None:
    super().check_consume()
    try:
      # Always consume regular animations first since movement animations need result
      for a in self.animations:
        a._consume(sprite_manager, animations)
      for da in self.movement_animations:
        da._consume(animations)

    except ParseException as ex:
      raise ParseException('Error in resource %s: %s' % (self, ex.msg))
